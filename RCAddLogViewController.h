//
//  RCRateItViewController.h
//  RiverChart2
//
//  Created by Tim Fiez on 4/15/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DYRateView.h"
#import "RCChartItem.h"

@interface RCAddLogViewController : UIViewController <DYRateViewDelegate, UITextViewDelegate, UITextFieldDelegate>
//- (void)changedToNewRate:(NSNumber *)rate;
@property (strong, nonatomic) IBOutlet UITextView *logTextView;
@property (strong, nonatomic) IBOutlet UITextField *dateTextField;
@property (strong, nonatomic) IBOutlet UILabel *locationLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (readonly, retain) UIToolbar *inputAccessoryView;
//@property (strong, nonatomic) IBOutlet UIView *scrollViewContainer;
@property (weak, nonatomic) RCChartItem *chartItem;
@property (strong, nonatomic) IBOutlet UIView *logTextViewContainer;
@end
