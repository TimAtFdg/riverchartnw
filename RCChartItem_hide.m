//
//  RCChartItem.m
//  RiverChart2
//
//  Created by Tim Fiez on 1/19/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCChartItem.h"

@implementation RCChartItem
@synthesize name=_name;
@synthesize PEType=_PEType;
@synthesize stationID=_stationID;
@synthesize hasDischargeData=_hasDischargeData;
@synthesize hasStageData=_hasStageData;

-(id)initWithDictionary:(NSDictionary *)chartItemData
{
    if(self = [super init])
    {
        _name = [chartItemData valueForKey:@"name"];
        _PEType = [chartItemData valueForKey:@"PEType"];
        _stationID = [chartItemData valueForKey:@"stationID"];
        _hasDischargeData = (BOOL)[chartItemData valueForKey:@"hasDischargeData"];
        _hasStageData = (BOOL)[chartItemData valueForKey:@"hasStageData"];
    }
    return self;
}
@end

