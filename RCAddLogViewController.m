//
//  RCRateItViewController.m
//  RiverChart2
//
//  Created by Tim Fiez on 4/15/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//
#define kRateViewWidth 200
#import "RCAddLogViewController.h"
#import "RCStation.h"
#import "RCChartItem.h"
#import "RCModelShared.h"
#import "RCLog.h"
#import "RCGlobalSingleton.h"
#import <QuartzCore/QuartzCore.h>
//#import "DYRateView.h"

@interface RCAddLogViewController ()
{
    float startingScrollViewOffset;
    float heightOfTextViewWorkingArea;
    NSDate *dateForLog;
    UIDatePicker *datePicker;
    NSDateFormatter *dateFormatter;
    DYRateView *rateView;
    //UIToolbar *inputAccessoryView;
}
@property id keyBoardObject;
@property bool keyBoardIsVisible;
@end

@implementation RCAddLogViewController

@synthesize locationLabel;
@synthesize dateTextField;
@synthesize logTextView;
@synthesize inputAccessoryView;
@synthesize scrollView;
//@synthesize scrollViewContainer;
@synthesize chartItem;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self registerForKeyboardNotifications];
    
    //DYRateView *rateView = [[DYRateView alloc] initWithFrame:CGRectMake(65, 40, 100, 14)];
    rateView = [[DYRateView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width-kRateViewWidth)/2, 36, kRateViewWidth, 21) fullStar:[UIImage imageNamed:@"StarFullLarge.png"] emptyStar:[UIImage imageNamed:@"StarEmptyLarge.png"]];
    rateView.rate = 0.0;
    rateView.padding = 20;
    rateView.alignment = RateViewAlignmentCenter;
    rateView.editable = YES;
    rateView.delegate = self;
    [self.scrollView addSubview:rateView];
    
    [[self.logTextViewContainer layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.logTextViewContainer layer] setBorderWidth:2.3];
    [[self.logTextViewContainer layer] setCornerRadius:15];
    //self.logTextView.inputAccessoryView = [self inputAccessoryView];
    
    // setup the date picker
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    self.dateTextField.inputView = datePicker;
    self.dateTextField.inputAccessoryView = [self inputAccessoryView];
    self.dateTextField.delegate = self;
    
    self.logTextView.delegate = self;
    //self.scrollView.contentSize = self.scrollViewContainer.frame.size;
    
    // set date field
    //NSString *MyString;
	dateForLog = [NSDate date];
	dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"EEEE MMMM d, yyyy"];
	//MyString = [dateFormatter stringFromDate:now];
	self.dateTextField.text = [dateFormatter stringFromDate:dateForLog];
    self.locationLabel.text = self.chartItem.rcStation_relationship.name;
    self.keyBoardIsVisible=NO;
    
//    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
//    self.logTextView.leftView = paddingView;
//    textField.leftViewMode = UITextFieldViewModeAlways;
//    self.logTextView.contentInset = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0);
//    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    paragraphStyle.headIndent = 20.0;
//    paragraphStyle.firstLineHeadIndent = 20.0;
//    paragraphStyle.tailIndent = -20.0;
//    
//    NSDictionary *attrsDictionary = @{NSFontAttributeName: [UIFont fontWithName:@"TrebuchetMS" size:12.0], NSParagraphStyleAttributeName: paragraphStyle};
//    self.logTextView.attributedText = [[NSAttributedString alloc] initWithString:@"" attributes:attrsDictionary];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)  name:UIDeviceOrientationDidChangeNotification  object:nil];

    [self adjustViewsForOrientation:nil];
}

-(void)viewDidAppear:(BOOL)animated
{

}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}
- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
//    if(self.keyBoardIsVisible)
//       [self textViewDidChange:nil];
}
- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    rateView.frame = CGRectMake((self.view.bounds.size.width-kRateViewWidth)/2, 36, kRateViewWidth, 21);
    return;
//    //CGSize size = self.logTextView.frame.size;
//    //CGSize sizeOfView = self.view.frame.size;
//    CGRect scrollViewRect = self.scrollView.frame;
//    CGRect rect = self.logTextViewContainer.frame;
//    CGRect rectOfTextView = self.logTextView.frame;
//
//    //scrollViewRect = CGRectMake(scrollViewRect.origin.x, scrollViewRect.origin.y, scrollViewRect.size.width, scrollViewRect.size.height-124-35);
//
//    rect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, scrollViewRect.size.height-124-35);
//    //rect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, sizeOfView.height-124-35-66);
//    //rectOfTextView = CGRectMake(rect.origin.x+5, rect.origin.y+5, rect.size.width-10, rect.size.height-10);
//    rectOfTextView = CGRectMake(rect.origin.x+5, rect.origin.y+5, rect.size.width-10, rect.size.height-10);
//
//    self.logTextViewContainer.frame = rect;
//    self.logTextView.frame = rectOfTextView;
//    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
//    {
//        rect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, scrollViewRect.size.height-124-35);
//        self.logTextViewContainer.frame = rect;
//    }
//    else if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
//    {
//        rect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, scrollViewRect.size.height-124-35);
//        self.logTextViewContainer.frame = rect;
//    }
}

-(IBAction)datePickerValueChanged:(id)sender
{
    dateForLog = [datePicker date];
	self.dateTextField.text = [dateFormatter stringFromDate:dateForLog];    
}

#pragma mark - DYRateViewDelegate
- (void)rateView:(DYRateView *)rateView changedToNewRate:(NSNumber *)rate {
    //NSString *temp = [NSString stringWithFormat:@"Rate: %d", rate.intValue];
    NSLog(@"Rate: %f", rate.floatValue);
}

//#pragma mark - Rotation
//-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    return true;// (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
//}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)cancelPressed:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)savePressed:(UIBarButtonItem *)sender {
    RCLog* newLog = [RCModelShared getNewLog];
    newLog.note = logTextView.text;
    newLog.rating = [NSNumber numberWithFloat:rateView.rate];//[NSNumber numberWithInt:Irint(rateView.rate)];
    newLog.date = dateForLog;
    newLog.peType = self.chartItem.peType;
    newLog.stationID = self.chartItem.stationID;
    newLog.rcStation_relationship = self.chartItem.rcStation_relationship;
    [RCModelShared updateLog];
    if([[RCGlobalSingleton globalSingleton] logTableView] != nil)
        [[[RCGlobalSingleton globalSingleton] logTableView] reloadData];

    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)viewDidUnload {
    [self setLogTextView:nil];
    [self setDateTextField:nil];
    [self setLocationLabel:nil];
    [self setScrollView:nil];
    //[self setScrollViewContainer:nil];
    [super viewDidUnload];
}

-(UIToolbar *)inputAccessoryView
{
    // from http://ankushasthana.blogspot.com -- input accessory view
    if(!inputAccessoryView)
    {
        inputAccessoryView = [[UIToolbar alloc]init];
        inputAccessoryView.tintColor = nil;
        inputAccessoryView.barStyle = UIBarStyleBlack;
        inputAccessoryView.translucent = YES;
        inputAccessoryView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem
                                        alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneOfAccessoryViewClicked)];
        inputAccessoryView.items = @[flexibleSpaceLeft,doneButton];
        
        //inputAccessoryView.frame = (CGRect){CGPointZero,  [self.inputAccessoryView sizeThatFits:CGSizeZero]};
        //CGPoint point = CGPointMake(CGRectGetWidth(self.view.bounds), 20);
        inputAccessoryView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 30);// (CGRect){CGPointZero, point};// [self.inputAccessoryView sizeThatFits:CGSizeZero]};

    }
    return inputAccessoryView;
}

-(void)doneOfAccessoryViewClicked
{
    if(self.logTextView.isFirstResponder)
        [logTextView resignFirstResponder];
    else
        [dateTextField resignFirstResponder];
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSLog(@"keyboard was shown");
    //self.keyBoardIsVisible=YES;
    //return;
    float buffer = 5;
    if(self.logTextView.isFirstResponder)
    {
        self.logTextView.inputAccessoryView = [self inputAccessoryView];
        NSDictionary* info = [aNotification userInfo];
        self.keyBoardObject = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
        //CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        float keyboardHeight = 0;
        if(UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation))
        //if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
            keyboardHeight = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
        else
            keyboardHeight = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.width;
        
        //keyboardHeight += 30; //inputAccessoryView.frame.size.height; // 44.0;
        
        float overallHeight = CGRectGetHeight(self.view.bounds);
        
        //NSLog(@"overallHeight: %f",overallHeight);
        //float logTextViewContainerHeight = overallHeight - self.logTextViewContainer.frame.origin.y-35-64;

        // resize log area
//        float overallHeight = 0;
//        if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
//            overallHeight = self.view.frame.size.height;
//        else
//            overallHeight = self.view.frame.size.width;
        //float overallHeight =  self.view.frame.size.height; // self.scrollView.frame.size.height;
        float logTextViewContainerHeight = overallHeight - keyboardHeight - 64-2*buffer;
        float logTextViewHeight = logTextViewContainerHeight - (self.logTextViewContainer.frame.size.height - self.logTextView.frame.size.height);
        self.logTextViewContainer.frame = CGRectMake(self.logTextViewContainer.frame.origin.x, self.logTextViewContainer.frame.origin.y, self.logTextViewContainer.frame.size.width, logTextViewContainerHeight);
        self.logTextView.frame = CGRectMake(self.logTextView.frame.origin.x, self.logTextView.frame.origin.y, self.logTextView.frame.size.width, logTextViewHeight);
        // then set scroll position
        CGPoint scrollPoint = scrollView.contentOffset;
        scrollPoint.y =self.logTextViewContainer.frame.origin.y-buffer;
        [scrollView setContentOffset:scrollPoint animated:YES];

        [self textViewDidChange:nil];
        
        return;
        
    }
}

// Called when the UIKeyboardDidShowNotification is sent.
//- (void)keyboardWasShown:(NSNotification*)aNotification
//{
//    //self.keyBoardIsVisible=YES;
//    //return;
//    if(self.logTextView.isFirstResponder)
//    {
//
//        NSDictionary* info = [aNotification userInfo];
//        self.keyBoardObject = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
//        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//        
//        float keyboardHeight = 0;
//        if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
//            keyboardHeight = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
//        else
//            keyboardHeight = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.width;
//        
//        keyboardHeight += inputAccessoryView.frame.size.height; // 44.0;
//
////        CGRect scrollViewFrame= self.scrollView.frame;
////        scrollViewFrame = CGRectMake(scrollViewFrame.origin.x, scrollViewFrame.origin.y, scrollViewFrame.size.width, scrollViewFrame.size.height+keyboardHeight);
////        self.scrollView.frame = scrollViewFrame;
//
//        
//        
//        [self textViewDidChange:nil];
//        
//        return;
//        
//        
//        // If active text field is hidden by keyboard, scroll it so it's visible
//        // Your application might not need or want this behavior.
//        //CGRect aRect = self.scrollViewContainer.frame;
//        //aRect.size.height -= kbSize.height;
//        //    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
//        
//        //    if (!CGRectContainsPoint(aRect, logTextView.frame.origin) ) {
//        //        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height);
//        //CGPoint scrollPoint = CGPointMake(0.0, logTextView.frame.origin.y-kbSize.height);
//        CGPoint scrollPoint = CGPointMake(0.0, self.logTextViewContainer.frame.origin.y-10); // 40
//        
//        // save info regarding visible height of text view
//        //heightOfTextViewWorkingArea = self.scrollView.frame.size.height - scrollPoint.y - kbSize.height;
//        heightOfTextViewWorkingArea = self.scrollView.frame.size.height - keyboardHeight;
//        
//        // save starting offset -- used in textviewdidchange
//        startingScrollViewOffset = scrollPoint.y;
//        //NSLog(@"Starting scrollViewOffet %f",startingScrollViewOffset);
//        
//        // handle case where user clicks somewhere other than first row of logTextView
////        if(logTextView.selectedTextRange!= nil)
////        {
////            CGPoint cursorPosition = [logTextView caretRectForPosition:logTextView.selectedTextRange.start].origin;
////            scrollPoint.y +=(cursorPosition.y-8);
////        }
//        [scrollView setContentOffset:scrollPoint animated:YES];
//    }
//}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.keyBoardIsVisible=NO;
    //self.logTextView.inputAccessoryView = nil;

//    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
//    scrollView.contentInset = contentInsets;
//    scrollView.scrollIndicatorInsets = contentInsets;
    
    // resize log area
    //float overallHeight =  self.view.frame.size.height; // self.scrollView.frame.size.height;
    float overallHeight = CGRectGetHeight(self.view.bounds);
    float logTextViewContainerHeight = overallHeight - self.logTextViewContainer.frame.origin.y-35-64;
    float logTextViewHeight = logTextViewContainerHeight - (self.logTextViewContainer.frame.size.height - self.logTextView.frame.size.height);
    self.logTextViewContainer.frame = CGRectMake(self.logTextViewContainer.frame.origin.x, self.logTextViewContainer.frame.origin.y, self.logTextViewContainer.frame.size.width, logTextViewContainerHeight);
    self.logTextView.frame = CGRectMake(self.logTextView.frame.origin.x, self.logTextView.frame.origin.y, self.logTextView.frame.size.width, logTextViewHeight);
    // then set scroll position
    CGPoint scrollPoint = scrollView.contentOffset;
    scrollPoint.y=0;// =self.logTextViewContainer.frame.origin.y-buffer;
    [scrollView setContentOffset:scrollPoint animated:YES];
    
    //[self textViewDidChange:nil];
    
    return;

}

-(void)textViewDidChange:(UITextView *)textView
{
//    return;
//    float topBuffer = 5;
//    float bufferToKeyboard = -5;
//    NSLog(@"scroll content offset %f",scrollView.contentOffset.y);
//
//    //CGSize kbSize = [self.keyBoardObject CGRectValue].size;
//    float keyboardHeight = 0;
//    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
//        keyboardHeight = [self.keyBoardObject CGRectValue].size.height;
//    else
//        keyboardHeight = [self.keyBoardObject CGRectValue].size.width;
//    
//    
//    heightOfTextViewWorkingArea = self.scrollView.frame.size.height - keyboardHeight-44; // 44 height of accessory view
//    NSLog(@"height of text view working area: %f",heightOfTextViewWorkingArea);
    
    // top of text view area
//    float textViewTop = self.logTextViewContainer.frame.origin.y;
    
    
    // height of content
//    float contentSizeHeight=self.logTextView.contentSize.height;
//    NSLog(@"content size height: %f",contentSizeHeight);
    
    // scroll position of textview
//    float textViewScrollPosition=self.logTextView.contentOffset.y;
//    NSLog(@"text view scroll position: %f",textViewScrollPosition);
    
    
    // if text view is not scrolling
//    if(logTextView.contentOffset.y==0 && cursorPosition<self.logTextView.frame.size.height) // no pending scroll
//    {
//        // measure from cursor position
//        float cursorAbsolutePosition = textViewTop+ cursorPosition;
//        float amountToScroll = (cursorAbsolutePosition - scrollView.contentOffset.y) - (heightOfTextViewWorkingArea -bufferToKeyboard);
//        NSLog(@"amountToScroll: %f", amountToScroll);
//        if(amountToScroll>0 || (scrollView.contentOffset.y+amountToScroll)>(textViewTop-topBuffer))
//        {
//            CGPoint scrollPoint = scrollView.contentOffset;
//            scrollPoint.y += roundf(amountToScroll);
//            [scrollView setContentOffset:scrollPoint animated:YES];
//        }
//            
//        
//    }
//    else
    NSLog(@"==============");
    float cursorPosition=0;
    CGPoint cursorPositionPoint = CGPointZero;
    if(self.logTextView.selectedTextRange!= nil)
    {
        cursorPosition = [logTextView caretRectForPosition:logTextView.selectedTextRange.start].origin.y;
        cursorPositionPoint = [logTextView caretRectForPosition:logTextView.selectedTextRange.start].origin;
        NSLog(@"cursorPosition: %f",cursorPosition);
        //scrollPoint.y +=(cursorPosition.y-8);
    }
    if(self.logTextView.contentOffset.y>0 || cursorPosition>self.logTextView.frame.size.height) // scrolling is occuring
    {
        // make sure cursor is visible within logTextView
        NSLog(@"log text view height: %f",self.logTextView.frame.size.height);
        NSLog(@"content offset y: %f", self.logTextView.contentOffset.y);
        NSLog(@"cursor position: %f",cursorPosition);
        //if(cursorPosition> (self.logTextView.frame.size.height-self.logTextView.contentOffset.y))
        if((cursorPosition - self.logTextView.contentOffset.y)> self.logTextView.frame.size.height-20)
        {
            NSLog(@"ADJ textview scroll +++++++++++++++++++++++++++++");
            CGPoint scrollPoint = self.logTextView.contentOffset;
            NSLog(@"scroll point y: %f",scrollPoint.y);
            float newScrollOffset =(cursorPosition - self.logTextView.contentOffset.y)- (self.logTextView.frame.size.height-20);
            NSLog(@"new scroll offset: %f",newScrollOffset);
            scrollPoint.y+=newScrollOffset;
            //scrollPoint.x = cursorPositionPoint.x;
            NSLog(@"new scroll point y: %f",scrollPoint.y);
            //scrollPoint.y+= roundf(cursorPosition -(self.logTextView.frame.size.height-self.logTextView.contentOffset.y));
            [self.logTextView setContentOffset:scrollPoint animated:YES];
            //[self.logTextView setContentOffset: animated:YES];
            NSLog(@"scroll point y after adj: %f",self.logTextView.contentOffset.y);

        }
        // now make sure cursor is in visible area
        // measure from cursor position
//        float cursorAbsolutePosition = textViewTop+ cursorPosition-self.logTextView.contentOffset.y;
//        float amountToScroll = (cursorAbsolutePosition - scrollView.contentOffset.y) - (heightOfTextViewWorkingArea -bufferToKeyboard);
//        NSLog(@"amountToScroll: %f", amountToScroll);
//        NSLog(@"or amount: %f, %f",scrollView.contentOffset.y+amountToScroll,textViewTop-topBuffer);
//        if(amountToScroll>0 || (scrollView.contentOffset.y+amountToScroll)>(textViewTop-topBuffer))
//        {
//            CGPoint scrollPoint = scrollView.contentOffset;
//            scrollPoint.y += roundf(amountToScroll);
//            [scrollView setContentOffset:scrollPoint animated:YES];
//        }
    }
    
//    if((scrollView.contentOffset.y+logTextView.contentOffset.y)> heightOfTextViewWorkingArea && logTextView.contentOffset.y==0)
//   // if(logTextView.contentSize.height>heightOfTextViewWorkingArea && scrollView.contentOffset.y >=startingScrollViewOffset && logTextView.contentSize.height < logTextView.frame.size.height)
//    {
//        float scrollChange = (scrollView.contentOffset.y+logTextView.contentOffset.y)-heightOfTextViewWorkingArea;
//       // float scrollChange = logTextView.contentSize.height - heightOfTextViewWorkingArea - (scrollView.contentOffset.y - startingScrollViewOffset)+10;
//        if(scrollChange>0)
//        {
//            CGPoint scrollPoint = scrollView.contentOffset;
//            scrollPoint.y += scrollChange;
//            [scrollView setContentOffset:scrollPoint animated:YES];
//        }
//    }
//    NSLog(@"text height %f",logTextView.contentSize.height);
}
@end
