//
//  RCChartItem.h
//  RiverChart2
//
//  Created by Tim Fiez on 1/19/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//
//howdy

#import <Foundation/Foundation.h>

@interface RCChartItem : NSObject
@property(nonatomic,strong,readonly) NSString* name;
@property(nonatomic,strong,readonly) NSString* PEType;
@property(nonatomic,strong,readonly) NSString* stationID;
@property(nonatomic,readonly) BOOL hasDischargeData;
@property(nonatomic,readonly) BOOL hasStageData;
-(id)initWithDictionary:(NSDictionary*)chartItemData;
@end
