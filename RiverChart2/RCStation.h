//
//  RCStation.h
//  RiverChart2
//
//  Created by Tim Fiez on 5/24/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RCChartItem;

@interface RCStation : NSManagedObject

@property (nonatomic, retain) NSString * stationID;
@property (nonatomic, retain) NSString * name;
@property (nonatomic) BOOL hasDischargeData;
@property (nonatomic) BOOL hasStageData;
@property (nonatomic, retain) NSSet *rcLog_relationship;
@property (nonatomic, retain) NSSet *rcChartItem_relationship;
-(id)setAttributesWithDictionary:(NSDictionary *)stationData;
@end

@interface RCStation (CoreDataGeneratedAccessors)

- (void)addRcLog_relationshipObject:(NSManagedObject *)value;
- (void)removeRcLog_relationshipObject:(NSManagedObject *)value;
- (void)addRcLog_relationship:(NSSet *)values;
- (void)removeRcLog_relationship:(NSSet *)values;

- (void)addRcChartItem_relationshipObject:(RCChartItem *)value;
- (void)removeRcChartItem_relationshipObject:(RCChartItem *)value;
- (void)addRcChartItem_relationship:(NSSet *)values;
- (void)removeRcChartItem_relationship:(NSSet *)values;

@end
