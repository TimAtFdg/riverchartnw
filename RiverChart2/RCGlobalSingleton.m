//
//  RCGlobalSingleton.m
//  RiverChart2
//
//  Created by Tim Fiez on 4/15/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCGlobalSingleton.h"
static RCGlobalSingleton *_globalSingleton;

@implementation RCGlobalSingleton
@synthesize favoritesTableView;
@synthesize logTableView;
+(void)initialize
{
    if (self == [RCGlobalSingleton class])
    {
        _globalSingleton = [[self alloc]init];
    }
}

+(id)globalSingleton
{
    return _globalSingleton;
}


@end
