//
//  RCFetchObservedChartData.h
//  RiverChart2
//
//  Created by Tim on 12/27/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCChartData.h"

@interface RCFetchObservedChartData : NSObject
+(RCChartData*)GetForStation:(NSString*)stationID PEType:(NSString*)PEType LogDate:(NSDate*)logDate;
@end

