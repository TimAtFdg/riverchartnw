//
//  RCLogNavigationController.h
//  RiverChart2
//
//  Created by Tim on 2/2/14.
//  Copyright (c) 2014 Tim Fiez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCLogNavigationController : UINavigationController
@property (weak, nonatomic) IBOutlet UITabBarItem *logTabBarItem;

@end
