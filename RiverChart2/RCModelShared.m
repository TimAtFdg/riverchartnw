//
//  RCModelShared.m
//  RiverChart2
//
//  Created by Tim Fiez on 1/19/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCModelShared.h"
#import "RCStation.h"
#import "RCConstants.h"
#define kTimeout 60.0

@interface RCModelShared()
+(NSString *)itemArchivePath;
//+(void)loadAllItems;
+(void)loadFavorites;
@end
static NSMutableArray *_logItems;
static NSMutableArray *_stationItems;
//static NSMutableArray *_PeTypeItems;
static NSMutableArray *_chartItems;
static NSArray *_favoriteChartItems;
static NSDictionary *_PETypeDefinitions;
static NSManagedObjectContext *context;
static NSManagedObjectModel *model;
static int _lastSelectedUnit;

@implementation RCModelShared
+(NSMutableArray *)stationItems
{
    return _stationItems;
}
+(NSMutableArray *)logItems
{
    return _logItems;
}
//+(NSMutableArray *)PeTypeItems
//{
//    return _PeTypeItems;
//}
+(NSMutableArray*)chartItems
{
    return _chartItems;
}
+(NSDictionary*)PETypeDefinitions
{
    return _PETypeDefinitions;
}
+(NSArray*)favoriteChartItems
{
    return _favoriteChartItems;
}
+(NSString *)itemArchivePath
{
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [documentDirectories objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:@"store.data"];
}
+(void)initialize
{
    if (self == [RCModelShared class])
    {
        if(!model || !context)
        {
            model = [NSManagedObjectModel mergedModelFromBundles:nil];
            NSPersistentStoreCoordinator *psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
            NSURL *storeURL = [NSURL fileURLWithPath:[self itemArchivePath]];
            
            
            //NSURL *_storeURL = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"MyAppSQLStore.sqlite"]];
            //NSPersistentStore *_store = [psc persistentStoreForURL:storeURL];
            //NSPersistentStore *store = [[psc persistentStores] objectAtIndex:0];
            
            NSError *error = nil;
            //            if(![psc removePersistentStore:store error:&error])
            //            {
            //                [NSException raise:@"remove failed" format:@"Reason: %@", [error localizedDescription]];
            //            }
            //            error=nil;
            
            
            if(![psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
            {
                [NSException raise:@"Open failed" format:@"Reason: %@", [error localizedDescription]];
            }
            context = [[NSManagedObjectContext alloc] init];
            [context setPersistentStoreCoordinator:psc];
            //[context reset];
            [context setUndoManager:nil];
        }
        
        if(!_chartItems)
        {
            // get values from database
            [self loadAllChartItems];
            [self loadAllStationItems];
            [self loadAllLogItems];
            [self loadFavorites];
            
            //bool refreshData = YES;
            bool saveData = YES;
            if(_chartItems==nil || _chartItems.count==0)
            {
                // get data from resource bundle
                NSString* path = [[NSBundle mainBundle] pathForResource:@"AllChartItems"
                                                                 ofType:@"json"];
                NSData *allChartItemsData = [NSData dataWithContentsOfFile:path];
                
                if(allChartItemsData==nil) // load from server
                {
                    
                    // get data from web request and insert into database
                    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:kGetAllChartItemsUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:kTimeout];
                    NSHTTPURLResponse *response = nil;
                    NSError *error = nil;
                    allChartItemsData = [NSURLConnection sendSynchronousRequest:req returningResponse:&response error:&error];
                    if(allChartItemsData!=nil)
                    {
                        // save data
                        if(saveData)
                        {
                            NSString *chartDataDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
                            NSString *filePath = [chartDataDirectory stringByAppendingPathComponent:@"AllChartItems.json"];
                            [allChartItemsData writeToFile:filePath options:nil error:nil];
                        }
                    }
                }
                if(allChartItemsData!=nil)
                {
                    //NSMutableArray *_chartItemsUpdate = [[NSMutableArray alloc] init];
                    NSError *error = nil;
                    NSArray *searchResults = [NSJSONSerialization JSONObjectWithData:allChartItemsData options:NSJSONReadingAllowFragments error:&error];
                    
                    if(searchResults != nil)
                    {
                        // delete all existing records from database
                        [self deleteAllItems];
                        
                        NSString *newStationID;
                        NSPredicate *predicateForStations;
                        NSArray *matchingItems;
                        RCStation *newStation;
                        RCStation *currentStation;
                        // rebuild list of stations
                        for(NSDictionary *searchResult in searchResults)
                        {
                            newStationID = [searchResult valueForKey:@"stationID"];
                            // look for match in existing stations
                            predicateForStations = [NSPredicate predicateWithFormat:@"stationID == %@",newStationID];
                            matchingItems = [_stationItems filteredArrayUsingPredicate:predicateForStations];
                            if (matchingItems == nil || [matchingItems count] == 0)
                            {
                                newStation = [NSEntityDescription insertNewObjectForEntityForName:@"RCStation" inManagedObjectContext:context];
                                newStation.stationID = [searchResult valueForKey:@"stationID"];
                                newStation.name = [searchResult valueForKey:@"name"];
                                newStation.hasDischargeData  = (BOOL)[searchResult valueForKey:@"hasDischargeData"];
                                newStation.hasStageData = (BOOL)[searchResult valueForKey:@"hasStageData"];
                                [_stationItems addObject:newStation];
                                currentStation = newStation;
                            }
                            else
                            {
                                if([matchingItems count]>1)
                                    [NSException raise:@"Initialize: find matching stations failed" format:nil];
                                currentStation = [matchingItems objectAtIndex:0];
                                
                            }
                            //    newChartItem.isFavorite = YES;
                            //else
                            
                            //
                            //  [_chartItemsUpdate addObject:[[RCChartItem alloc] initWithDictionary:searchResult]];
                            //}
                            //for (RCChartItem *chartItem in _chartItemsUpdate) {
                            RCChartItem *newChartItem = [NSEntityDescription insertNewObjectForEntityForName:@"RCChartItem" inManagedObjectContext:context];
                            
                            //self.name = [chartItemData valueForKey:@"name"];
                            //self.peType = [chartItemData valueForKey:@"PEType"];
                            //self.stationID = [chartItemData valueForKey:@"stationID"];
                            //self.hasDischargeData = (BOOL)[chartItemData valueForKey:@"hasDischargeData"];
                            //self.hasStageData = (BOOL)[chartItemData valueForKey:@"hasStageData"];
                            //self.isFavorite = NO;
                            
                            
                            //newChartItem.name = [searchResult valueForKey:@"name"];
                            newChartItem.peType = [searchResult valueForKey:@"PEType"];
                            newChartItem.stationID = [searchResult valueForKey:@"stationID"];
                            newChartItem.rcStation_relationship = currentStation;
                            newChartItem.isFavorite = NO;
                            //newChartItem.hasDischargeData =(BOOL)[searchResult valueForKey:@"hasDischargeData"];
                            //newChartItem.hasStageData = (BOOL)[searchResult valueForKey:@"hasStageData"];
                            
                            // look for match in old data
                            //   NSPredicate *predicate = [NSPredicate predicateWithFormat:@"stationID == %@ AND peType == %@ and //isFavorite == YES",newChartItem.stationID,newChartItem.peType];
                            //   NSArray *matchingItems = [_chartItems filteredArrayUsingPredicate:predicate];
                            // if there is a match
                            // update isBookmark status to reflect previously saved items
                            //if (matchingItems != nil || [matchingItems count] > 0)
                            //    newChartItem.isFavorite = YES;
                            //else
                            //   newChartItem.isFavorite = NO;
                        }
                        [self saveChanges];
                        // get values from database
                        [self loadAllChartItems];
                        [self loadAllStationItems];
                        [self loadAllLogItems];
                        [self loadFavorites];
                    }
                }
            }
        }
    }
    if(!_PETypeDefinitions)
    {
        _PETypeDefinitions = [NSDictionary dictionaryWithObjectsAndKeys:
                              @"Elevation, Project Powerhouse Forebay",@"HF",
                              @"River Stage",@"HG",
                              @"Elevation, Natural Lake",@"HL",
                              @"Height of Tide",@"HM",
                              @"Elevation, Pool",@"HP",
                              @"Lake Storage",@"LS",
                              @"Inflow",@"QI",
                              @"River Discharge", @"QR",
                              @"Water Temperature",@"TW",
                              @"Water Velocity", @"WV",  nil];
    }
    // get saved unit selection
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"LastSelectedUnit"]==nil)
        _lastSelectedUnit = 1; // default to second option
    else
    {
        NSNumber *savedUnitSelection = [defaults objectForKey:@"LastSelectedUnit"];
        _lastSelectedUnit = savedUnitSelection.intValue;
    }
}

+(void)loadAllChartItems
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *e = [[model entitiesByName] objectForKey:@"RCChartItem"];
    [request setEntity:e];
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"rcStation_relationship.name" ascending:YES];
    [request setSortDescriptors:[NSArray arrayWithObject:sd]];
    NSError *error;
    NSArray *result = [context executeFetchRequest:request error:&error];
    if(!result)
    {
        [NSException raise:@"Fetch failed" format:@"Reason: %@", [error localizedDescription]];
    }
    
    _chartItems = [[NSMutableArray alloc] initWithArray:result];
    
}

+(void)loadAllStationItems
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *e = [[model entitiesByName] objectForKey:@"RCStation"];
    [request setEntity:e];
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [request setSortDescriptors:[NSArray arrayWithObject:sd]];
    NSError *error;
    NSArray *result = [context executeFetchRequest:request error:&error];
    if(!result)
    {
        [NSException raise:@"Fetch failed" format:@"Reason: %@", [error localizedDescription]];
    }
    
    _stationItems = [[NSMutableArray alloc] initWithArray:result];
    
}
+(void)loadAllLogItems
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *e = [[model entitiesByName] objectForKey:@"RCLog"];
    [request setEntity:e];
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO];
    [request setSortDescriptors:[NSArray arrayWithObject:sd]];
    NSError *error;
    NSArray *result = [context executeFetchRequest:request error:&error];
    if(!result)
    {
        [NSException raise:@"Fetch failed" format:@"Reason: %@", [error localizedDescription]];
    }
    
    _logItems = [[NSMutableArray alloc] initWithArray:result];
    
}
+(void)loadFavorites
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isFavorite == YES"];
    _favoriteChartItems = [_chartItems filteredArrayUsingPredicate:predicate];

}
+(void)deleteLogItem:(RCLog*)logItem
{
    [context deleteObject:logItem];
    [self updateLog];
}
+(BOOL)deleteAllItems
{
    for (NSManagedObject *chartItem in _chartItems) {
        [context deleteObject:chartItem];
    }
    for (NSManagedObject *logItem in _logItems) {
        [context deleteObject:logItem];
    }
    for (NSManagedObject *stationItem in _stationItems) {
        [context deleteObject:stationItem];
    }

//    for (NSManagedObject *PeTypeItem in _PeTypeItems) {
//        [context deleteObject:PeTypeItem];
//    }
    return [self saveChanges];
}
+(BOOL)saveChanges
{
    NSError *err = nil;
    bool successful = [context save:&err];
    if(!successful)
    {
        NSLog(@"Error saving: %@",[err localizedDescription]);
    }
    return successful;
}
+(void)updateFavorites
{
    [self saveChanges];
    [self loadFavorites];
}
+(RCLog*)getNewLog
{
    RCLog *newLog = [NSEntityDescription insertNewObjectForEntityForName:@"RCLog" inManagedObjectContext:context];
    return newLog;
}
+(void)updateLog
{
    [self saveChanges];
    [self loadAllLogItems];
}

+(RCChartItem*)fetchChartItemForStationId:(NSString*)stationID PeType:(NSString*)peType
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"stationID==%@ && peType==%@",stationID,peType];
    NSUInteger index = [_chartItems indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [predicate evaluateWithObject:obj];
    }];
    return [_chartItems objectAtIndex:index];
}
+(int)lastSelectedUnit
{
    return _lastSelectedUnit;
}
+(void)setLastSelectedUnit:(int)lastSelectedUnit
{
    _lastSelectedUnit = lastSelectedUnit;
}
@end
