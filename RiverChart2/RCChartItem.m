//
//  RCChartItem.m
//  RiverChart2
//
//  Created by Tim Fiez on 5/27/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCChartItem.h"
#import "RCStation.h"


@implementation RCChartItem

@dynamic isFavorite;
@dynamic peType;
@dynamic stationID;
@dynamic rcStation_relationship;

@end
