//
//  RCTabViewController.h
//  RiverChart2
//
//  Created by Tim Fiez on 3/11/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCTabViewController : UITabBarController

@end
