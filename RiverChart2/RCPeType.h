//
//  RCPeType.h
//  RiverChart2
//
//  Created by Tim Fiez on 5/22/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface RCPeType : NSManagedObject

@property (nonatomic, retain) NSString * stationID;
@property (nonatomic, retain) NSString * peType;
@property (nonatomic) BOOL isFavorite;
@property (nonatomic, retain) NSManagedObject *rcStation_relationship;

@end
