//
//  RCAppDelegate.m
//  RiverChart2
//
//  Created by Tim Fiez on 12/28/12.
//  Copyright (c) 2012 Tim Fiez. All rights reserved.
//

#import "RCAppDelegate.h"
#import "RCModelShared.h"
#import "RCModelShared.h"
#import "Reachability.h"
#import "RCGlobalSingleton.h"

@interface RCAppDelegate()
@property Reachability *hostReach;
@end

@implementation RCAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    if([RCModelShared favoriteChartItems]==nil || [RCModelShared favoriteChartItems].count==0)
    {
        UITabBarController *tabBar = (UITabBarController *)self.window.rootViewController;
        tabBar.selectedIndex = 1;
    }
    
    
    // Observe the kNetworkReachabilityChangedNotification. When that notification is posted, the
    // method "reachabilityChanged" will be called.
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
    
    //Change the host name here to change the server your monitoring
    //remoteHostLabel.text = [NSString stringWithFormat: @"Remote Host: %@", @"www.apple.com"];
    _hostReach = [Reachability reachabilityWithHostName: @"www.fiezdevelopmentgroup.com"];
    [_hostReach startNotifier];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    // save last selected user selection
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:[RCModelShared lastSelectedUnit]] forKey:@"LastSelectedUnit"];
    [defaults synchronize];
    [self removeOldCachedFiles];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [[RCGlobalSingleton globalSingleton] setNetworkStatus:[curReach currentReachabilityStatus]];
}

-(void)removeOldCachedFiles
{
    dispatch_queue_t saveCustomQueue;
    saveCustomQueue = dispatch_queue_create("com.fiezdevelopmentgroup.RemoveOldCachedFilesCustomQueue", NULL);
    dispatch_async(saveCustomQueue, ^{
        
        // remove chart data older than 48 hours
        //NSString *chartDataDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/ChartData"];
        NSString *chartDataDirectory = [NSString stringWithFormat:@"%@/ChartData",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]];

        NSDirectoryEnumerator *directoryEnumerator = [[NSFileManager defaultManager] enumeratorAtPath:chartDataDirectory];
        
        NSDate *twoDaysAgo = [NSDate dateWithTimeIntervalSinceNow:(-60*60*48)];
        
        for (NSString *path in directoryEnumerator) {
            
            if ([[path pathExtension] isEqualToString:@"rtfd"]) {
                // Don't enumerate this directory.
                [directoryEnumerator skipDescendents];
            }
            else {
                
                NSDictionary *attributes = [directoryEnumerator fileAttributes];
                NSDate *fileModificationDate = [attributes objectForKey:NSFileModificationDate];
                
                if ([twoDaysAgo earlierDate:fileModificationDate] == fileModificationDate) {
                    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
                }
            }
        }
        
        // remove chart log data older than 168 hours (7 days)
        
        //chartDataDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/ChartLogData"];
        chartDataDirectory = [NSString stringWithFormat:@"%@/ChartLogData",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]];

        directoryEnumerator = [[NSFileManager defaultManager] enumeratorAtPath:chartDataDirectory];
        
        NSDate *sevenDaysAgo = [NSDate dateWithTimeIntervalSinceNow:(-60*60*168)];
        
        for (NSString *path in directoryEnumerator) {
            
            if ([[path pathExtension] isEqualToString:@"rtfd"]) {
                // Don't enumerate this directory.
                [directoryEnumerator skipDescendents];
            }
            else {
                
                NSDictionary *attributes = [directoryEnumerator fileAttributes];
                NSDate *fileModificationDate = [attributes objectForKey:NSFileModificationDate];
                
                if ([sevenDaysAgo earlierDate:fileModificationDate] == fileModificationDate) {
                    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
                }
            }
        }
    });
    
}

@end
