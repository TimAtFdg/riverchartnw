//
//  RCViewLogViewController.m
//  RiverChart2
//
//  Created by Tim on 12/16/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//
#define kRateViewWidth 200
#import "RCStation.h"
#import "RCChartItem.h"
#import "RCModelShared.h"
#import "RCLog.h"
#import "RCGlobalSingleton.h"
#import <QuartzCore/QuartzCore.h>
#import "RCViewLogViewController.h"
#import "RCScatterPlotViewController.h"

@interface RCViewLogViewController ()
{
    float startingScrollViewOffset;
    float heightOfTextViewWorkingArea;
    NSDate *dateForLog;
    UIDatePicker *datePicker;
    NSDateFormatter *dateFormatter;
    DYRateView *rateView;
    //UIToolbar *inputAccessoryView;
}
@property id keyBoardObject;
@property bool keyBoardIsVisible;
//@property (nonatomic)  UIToolbar* inputAccessoryView;
//@property (readonly, retain) UIToolbar *inputAccessoryView;
@end

@implementation RCViewLogViewController
@synthesize inputAccessoryView=_inputAccessoryView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self registerForKeyboardNotifications];
    
    //DYRateView *rateView = [[DYRateView alloc] initWithFrame:CGRectMake(65, 40, 100, 14)];
    rateView = [[DYRateView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width-kRateViewWidth)/2, 5, kRateViewWidth, 21) fullStar:[UIImage imageNamed:@"StarFullLarge.png"] emptyStar:[UIImage imageNamed:@"StarEmptyLarge.png"]];
    if(self.logItem!=nil)
        rateView.rate = [self.logItem.rating floatValue];
    else
        rateView.rate = 0.0;
    rateView.padding = 20;
    rateView.alignment = RateViewAlignmentCenter;
    rateView.editable = NO;
    //rateView.delegate = self;
    [self.scrollView addSubview:rateView];
    
    [[self.logTextViewContainer layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.logTextViewContainer layer] setBorderWidth:2.3];
    [[self.logTextViewContainer layer] setCornerRadius:15];
    
    self.logTextView.text = self.logItem.note;
    
    //self.logTextView.inputAccessoryView = [self inputAccessoryView];
    
//    // setup the date picker
//    datePicker = [[UIDatePicker alloc] init];
//    datePicker.datePickerMode = UIDatePickerModeDate;
//    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
//    self.dateTextField.inputView = datePicker;
//    self.dateTextField.inputAccessoryView = [self inputAccessoryView];
//    self.dateTextField.delegate = self;
//    
    self.logTextView.delegate = self;
//    //self.scrollView.contentSize = self.scrollViewContainer.frame.size;
    
    // set date field
    //NSString *MyString;
	dateForLog = self.logItem.date;
	dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"EEEE MMMM d, yyyy"];
	//MyString = [dateFormatter stringFromDate:now];
	self.dateTextField.text = [dateFormatter stringFromDate:dateForLog];
    self.locationLabel.text = self.logItem.rcStation_relationship.name;
    self.keyBoardIsVisible=NO;
    self.navigationItem.title=self.logItem.rcStation_relationship.name;
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)  name:UIDeviceOrientationDidChangeNotification  object:nil];
    
    [self adjustViewsForOrientation:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}
- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    //    if(self.keyBoardIsVisible)
    //       [self textViewDidChange:nil];
}
- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    rateView.frame = CGRectMake((self.view.bounds.size.width-kRateViewWidth)/2, 5, kRateViewWidth, 21);
}

//-(IBAction)datePickerValueChanged:(id)sender
//{
//    dateForLog = [datePicker date];
//	self.dateTextField.text = [dateFormatter stringFromDate:dateForLog];
//}

#pragma mark - DYRateViewDelegate
//- (void)rateView:(DYRateView *)rateView changedToNewRate:(NSNumber *)rate {
//    //NSString *temp = [NSString stringWithFormat:@"Rate: %d", rate.intValue];
//    NSLog(@"Rate: %f", rate.floatValue);
//}

//#pragma mark - Rotation
//-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    return true;// (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
//}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//- (IBAction)cancelPressed:(UIBarButtonItem *)sender {
//    [self dismissViewControllerAnimated:YES completion:NULL];
//}

//- (IBAction)savePressed:(UIBarButtonItem *)sender {
//    RCLog* newLog = [RCModelShared getNewLog];
//    newLog.note = self.logTextView.text;
//    newLog.rating = [NSNumber numberWithFloat:rateView.rate];//[NSNumber numberWithInt:Irint(rateView.rate)];
//    newLog.date = dateForLog;
//    newLog.rcStation_relationship = self.chartItem.rcStation_relationship;
//    [RCModelShared updateLog];
//    if([[RCGlobalSingleton globalSingleton] logTableView] != nil)
//        [[[RCGlobalSingleton globalSingleton] logTableView] reloadData];
//    
//    [self dismissViewControllerAnimated:YES completion:NULL];
//}

- (void)viewDidUnload {
    [self setLogTextView:nil];
    [self setDateTextField:nil];
    [self setLocationLabel:nil];
    [self setScrollView:nil];
    //[self setScrollViewContainer:nil];
    [super viewDidUnload];
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.logItem.note = self.logTextView.text;
    [RCModelShared updateLog];
}

-(UIToolbar *)inputAccessoryView
{
    // from http://ankushasthana.blogspot.com -- input accessory view
    if(!_inputAccessoryView)
    {
        _inputAccessoryView = [[UIToolbar alloc]init];
        _inputAccessoryView.tintColor = nil;
        _inputAccessoryView.barStyle = UIBarStyleBlack;
        _inputAccessoryView.translucent = YES;
        _inputAccessoryView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem
                                        alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneOfAccessoryViewClicked)];
        _inputAccessoryView.items = @[flexibleSpaceLeft,doneButton];
        
        //inputAccessoryView.frame = (CGRect){CGPointZero,  [self.inputAccessoryView sizeThatFits:CGSizeZero]};
        //CGPoint point = CGPointMake(CGRectGetWidth(self.view.bounds), 20);
        _inputAccessoryView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 30);// (CGRect){CGPointZero, point};// [self.inputAccessoryView sizeThatFits:CGSizeZero]};
        
    }
    return _inputAccessoryView;
}

-(void)doneOfAccessoryViewClicked
{
    if(self.logTextView.isFirstResponder)
        [self.logTextView resignFirstResponder];
    else
        [self.dateTextField resignFirstResponder];
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    //self.keyBoardIsVisible=YES;
    //return;
    float buffer = 5;
    if(self.logTextView.isFirstResponder)
    {
        self.logTextView.inputAccessoryView = [self inputAccessoryView];
        NSDictionary* info = [aNotification userInfo];
        self.keyBoardObject = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
        //CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        float keyboardHeight = 0;
        if(UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation))
        //if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
            keyboardHeight = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
        else
            keyboardHeight = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.width;
        
        //keyboardHeight += 30; //inputAccessoryView.frame.size.height; // 44.0;
        
        float overallHeight = CGRectGetHeight(self.view.bounds);
        //NSLog(@"overallHeight: %f",overallHeight);

        float logTextViewContainerHeight = overallHeight - keyboardHeight - 64-2*buffer;
        float logTextViewHeight = logTextViewContainerHeight - (self.logTextViewContainer.frame.size.height - self.logTextView.frame.size.height);
        self.logTextViewContainer.frame = CGRectMake(self.logTextViewContainer.frame.origin.x, self.logTextViewContainer.frame.origin.y, self.logTextViewContainer.frame.size.width, logTextViewContainerHeight);
        self.logTextView.frame = CGRectMake(self.logTextView.frame.origin.x, self.logTextView.frame.origin.y, self.logTextView.frame.size.width, logTextViewHeight);
        // then set scroll position
        CGPoint scrollPoint = self.scrollView.contentOffset;
        scrollPoint.y =self.logTextViewContainer.frame.origin.y-buffer;
        [self.scrollView setContentOffset:scrollPoint animated:YES];
        
        [self textViewDidChange:nil];
        
        return;
        
    }
}



// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.keyBoardIsVisible=NO;
    //self.logTextView.inputAccessoryView = nil;
    
    //    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    //    scrollView.contentInset = contentInsets;
    //    scrollView.scrollIndicatorInsets = contentInsets;
    
    // resize log area
    //float overallHeight =  self.view.frame.size.height; // self.scrollView.frame.size.height;
    float overallHeight = CGRectGetHeight(self.view.bounds);
    float logTextViewContainerHeight = overallHeight - self.logTextViewContainer.frame.origin.y-35-64;
    float logTextViewHeight = logTextViewContainerHeight - (self.logTextViewContainer.frame.size.height - self.logTextView.frame.size.height);
    self.logTextViewContainer.frame = CGRectMake(self.logTextViewContainer.frame.origin.x, self.logTextViewContainer.frame.origin.y, self.logTextViewContainer.frame.size.width, logTextViewContainerHeight);
    self.logTextView.frame = CGRectMake(self.logTextView.frame.origin.x, self.logTextView.frame.origin.y, self.logTextView.frame.size.width, logTextViewHeight);
    // then set scroll position
    CGPoint scrollPoint = self.scrollView.contentOffset;
    scrollPoint.y=0;// =self.logTextViewContainer.frame.origin.y-buffer;
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    
    //[self textViewDidChange:nil];
    
    return;
    
}

-(void)textViewDidChange:(UITextView *)textView
{
    NSLog(@"==============");
    float cursorPosition=0;
    CGPoint cursorPositionPoint = CGPointZero;
    if(self.logTextView.selectedTextRange!= nil)
    {
        cursorPosition = [self.logTextView caretRectForPosition:self.logTextView.selectedTextRange.start].origin.y;
        cursorPositionPoint = [self.logTextView caretRectForPosition:self.logTextView.selectedTextRange.start].origin;
        NSLog(@"cursorPosition: %f",cursorPosition);
        //scrollPoint.y +=(cursorPosition.y-8);
    }
    if(self.logTextView.contentOffset.y>0 || cursorPosition>self.logTextView.frame.size.height) // scrolling is occuring
    {
        // make sure cursor is visible within logTextView
        NSLog(@"log text view height: %f",self.logTextView.frame.size.height);
        NSLog(@"content offset y: %f", self.logTextView.contentOffset.y);
        NSLog(@"cursor position: %f",cursorPosition);
        //if(cursorPosition> (self.logTextView.frame.size.height-self.logTextView.contentOffset.y))
        if((cursorPosition - self.logTextView.contentOffset.y)> self.logTextView.frame.size.height-20)
        {
            NSLog(@"ADJ textview scroll +++++++++++++++++++++++++++++");
            CGPoint scrollPoint = self.logTextView.contentOffset;
            NSLog(@"scroll point y: %f",scrollPoint.y);
            float newScrollOffset =(cursorPosition - self.logTextView.contentOffset.y)- (self.logTextView.frame.size.height-20);
            NSLog(@"new scroll offset: %f",newScrollOffset);
            scrollPoint.y+=newScrollOffset;
            //scrollPoint.x = cursorPositionPoint.x;
            NSLog(@"new scroll point y: %f",scrollPoint.y);
            //scrollPoint.y+= roundf(cursorPosition -(self.logTextView.frame.size.height-self.logTextView.contentOffset.y));
            [self.logTextView setContentOffset:scrollPoint animated:YES];
            //[self.logTextView setContentOffset: animated:YES];
            NSLog(@"scroll point y after adj: %f",self.logTextView.contentOffset.y);
            
        }

    }
    

}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"showChartForLog"]){
        RCScatterPlotViewController *scatterPlotViewController = [segue destinationViewController];

        RCChartItem *chartItem = [RCModelShared fetchChartItemForStationId:self.logItem.stationID PeType:self.logItem.peType];
        
        scatterPlotViewController.PEType = chartItem.peType;
        scatterPlotViewController.stationID = chartItem.stationID;
        scatterPlotViewController.itemName = chartItem.rcStation_relationship.name;
        scatterPlotViewController.hidesBottomBarWhenPushed = true;
        scatterPlotViewController.chartItem = chartItem;
        scatterPlotViewController.isLogPlot=YES;
        scatterPlotViewController.logDate = self.logItem.date;
    }    
}

@end
