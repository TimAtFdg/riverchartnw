//
//  RCSearchTableViewController.m
//  RiverChart2
//
//  Created by Tim Fiez on 1/22/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCFavoritesTableViewController.h"
#import "RCModelShared.h"
#import "RCChartItem.h"
#import "RCStation.h"
#import "RCScatterPlotViewController.h"
#import "RCGlobalSingleton.h"

@interface RCFavoritesTableViewController ()
//@property (strong, nonatomic) NSMutableArray *filteredTableData;
//@property (strong, nonatomic) NSMutableDictionary *filteredDataConversionToFullData;
//@property bool isFiltered;
@end

@implementation RCFavoritesTableViewController
//@synthesize filteredTableData = _filteredTableData;
//@synthesize filteredDataConversionToFullData = _filteredDataConversionToFullData;
//@synthesize searchBar = _searchBar;
@synthesize chartItemTableView = _chartItemTableView;
//@synthesize isFiltered;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // save pointer to table view so I can update data when person adds to favorites
    [[RCGlobalSingleton globalSingleton] setFavoritesTableView:self.tableView];
    
//    isFiltered = NO;
//    self.searchBar.delegate = (id)self;
//    [self.searchBar showsCancelButton];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

//-(void)viewWillAppear:(BOOL)animated
//{
//    [self.tableView reloadData];
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if(isFiltered)
//        return [self.filteredTableData count];
//    else
    if([RCModelShared favoriteChartItems] == nil)
        return 0;
    else
        return [[RCModelShared favoriteChartItems] count];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *result = nil;
        static NSString *TableViewCellIdentifier = @"ChartItemCell";
        result = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
        if(result == nil)
        {
            result = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:TableViewCellIdentifier];
        }
        result.textLabel.font = [UIFont systemFontOfSize:14.0];

//        if(isFiltered)
//        {
//            result.textLabel.text =  ((RCChartItem *)[self.filteredTableData objectAtIndex:indexPath.row]).name;
//            //result.textLabel.text = @"SILETZ AT SILETZ";
//            result.detailTextLabel.text =  (NSString*)[[RCModelShared PETypeDefinitions] objectForKey:((RCChartItem *)[self.filteredTableData objectAtIndex:indexPath.row]).peType];
//        }
//        else
//        {
            //result.textLabel.text = [NSStrinringWithFormat:@"%ld", (long)indexPath.row];
    //RCStation* tempStation = ((RCChartItem *)[[RCModelShared favoriteChartItems] objectAtIndex:indexPath.row]).rcStation_relationship;
    
    result.textLabel.text = [((RCChartItem *)[[RCModelShared favoriteChartItems] objectAtIndex:indexPath.row]).rcStation_relationship name];
            result.detailTextLabel.text = (NSString*)[[RCModelShared PETypeDefinitions] objectForKey:((RCChartItem *)[[RCModelShared chartItems] objectAtIndex:indexPath.row]).peType];
        //}
    return result;

}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        RCChartItem *chartItem= (RCChartItem *)[[RCModelShared favoriteChartItems] objectAtIndex:indexPath.row];

        // Delete the row from the data source
       // [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        //if(!chartItem.isFavorite)
        //{
            chartItem.isFavorite = NO;
            [RCModelShared updateFavorites];
        [self.tableView reloadData];
            
            
        //}
    }   
//    else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

//#pragma mark - Table view delegate

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Navigation logic may go here. Create and push another view controller.
//    /*
//     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
//     // ...
//     // Pass the selected object to the new view controller.
//     [self.navigationController pushViewController:detailViewController animated:YES];
//     */
//}
//-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString *)searchText
//{
//    if([searchText length]==0)
//    {
//        isFiltered = NO;
//        //[self.searchBar resignFirstResponder];
//    }
//    else {
//        isFiltered = YES;
//        self.filteredTableData = [[NSMutableArray alloc] init];
//        self.filteredDataConversionToFullData = [[NSMutableDictionary alloc] init];
//        NSUInteger index = 0;
//        NSUInteger filteredTableIndex =0;
//        for(RCChartItem *chartItem in [RCModelShared chartItems])
//        {
//            NSRange nameRange = [chartItem.name rangeOfString:searchText options:NSCaseInsensitiveSearch];
//            if(nameRange.location!=NSNotFound)
//            {
//                [self.filteredTableData addObject:chartItem];
//                [self.filteredDataConversionToFullData setObject:[NSNumber numberWithUnsignedInteger:index] forKey:[NSNumber numberWithUnsignedInteger:filteredTableIndex]];
//                filteredTableIndex++;
//            }
//            index++;
//        }
//    }
//    [self.chartItemTableView reloadData];
//    //NSLog(@"search text: %@",searchText);
//}

//-(void)searchBarSearchButtonClicked:(UISearchBar*)searchBar
//{
//    [searchBar resignFirstResponder];
//}

//-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
//{
//    [self.searchBar resignFirstResponder];
//}

- (void)viewDidUnload {
    //[self setSearchBar:nil];
    [self setChartItemTableView:nil];
    [super viewDidUnload];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"showChartForSelection"]){
        RCScatterPlotViewController *scatterPlotViewController = [segue destinationViewController];
        //scatterPlotViewController

        int selectedRowIndex =[self.tableView indexPathForSelectedRow].row;
        RCChartItem *chartItem;
//        if(self.isFiltered)
//        {
//            chartItem= (RCChartItem *)[self.filteredTableData objectAtIndex:selectedRowIndex];
//        }
//        else
//        {
            chartItem= (RCChartItem *)[[RCModelShared favoriteChartItems] objectAtIndex:selectedRowIndex];
        //}
        
        scatterPlotViewController.PEType = chartItem.peType;
        scatterPlotViewController.stationID = chartItem.stationID;
        scatterPlotViewController.itemName = chartItem.rcStation_relationship.name;
        scatterPlotViewController.hidesBottomBarWhenPushed = true;
        scatterPlotViewController.chartItem = chartItem;
        scatterPlotViewController.isLogPlot = NO;
    }

}
@end
