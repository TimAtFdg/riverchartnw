//
//  RCDateTimeDataPoint.h
//  RiverChart2
//
//  Created by Tim Fiez on 2/20/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RCDateTimeDataPoint : NSObject
@property (nonatomic) NSDate *XValue;
@property (nonatomic) double YValue;
-(id)initWithXValueAndYValue:(NSDate*)XValue YValue:(double)YValue;
@end
