//
//  RCModelShared.h
//  RiverChart2
//
//  Created by Tim Fiez on 1/19/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "RCLog.h"
#import "RCChartItem.h"

@interface RCModelShared : NSObject
+(NSMutableArray *)chartItems;
+(NSMutableArray *)stationItems;
+(NSDictionary *)PETypeDefinitions;
+(NSMutableArray *)favoriteChartItems;
+(NSMutableArray *)logItems;
+(int)lastSelectedUnit;
+(void)setLastSelectedUnit:(int)lastSelectedUnit;
//+(NSMutableArray *)PeTypeItems;
+(BOOL)saveChanges;
+(void)updateFavorites;
+(RCLog*)getNewLog;
+(void)updateLog;
+(void)deleteLogItem:(RCLog*)logItem;
+(RCChartItem*)fetchChartItemForStationId:(NSString*)stationID PeType:(NSString*)peType;
@end
