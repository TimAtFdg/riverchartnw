//
//  RCSearchViewController.m
//  RiverChart2
//
//  Created by Tim Fiez on 4/4/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCSearchViewController.h"
#import "RCModelShared.h"
#import "RCChartItem.h"
#import "RCStation.h"
#import "RCScatterPlotViewController.h"

@interface RCSearchViewController ()
@property (strong, nonatomic) NSMutableArray *filteredTableData;
@property (strong, nonatomic) NSMutableDictionary *filteredDataConversionToFullData;
@property bool isFiltered;
@end

@implementation RCSearchViewController
@synthesize filteredTableData = _filteredTableData;
@synthesize filteredDataConversionToFullData = _filteredDataConversionToFullData;
@synthesize searchBar = _searchBar;
@synthesize chartItemTableView = _chartItemTableView;
@synthesize isFiltered;

//- (id)initWithStyle:(UITableViewStyle)style
//{
//    self = [super initWithStyle:style];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

-(UITableView *)chartItemTableView
{
    if(!_chartItemTableView)
    {
        _chartItemTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _chartItemTableView.dataSource = self;
        CGRect tfFrame = [_chartItemTableView frame];
        [_chartItemTableView setFrame:CGRectMake(tfFrame.origin.x, tfFrame.origin.y+44+64, tfFrame.size.width, tfFrame.size.height-44-64-49)];  // -49 = tab bar
        _chartItemTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    
    return _chartItemTableView;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    isFiltered = NO;
    self.searchBar.delegate = (id)self;
    [self.searchBar showsCancelButton];
    [self.view addSubview:self.chartItemTableView];
    self.chartItemTableView.delegate = (id)self;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

//-(void)viewDidDisappear:(BOOL)animated
//{
//    if(_HUD!=nil)
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isFiltered)
        return [self.filteredTableData count];
    else
        return [[RCModelShared chartItems] count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *result = nil;
    static NSString *TableViewCellIdentifier = @"ChartItemCell";
    result = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
    if(result == nil)
    {
        result = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:TableViewCellIdentifier];
        //result.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    }
    result.textLabel.font = [UIFont systemFontOfSize:14.0];
    
    if(isFiltered)
    {
        result.textLabel.text =  ((RCChartItem *)[self.filteredTableData objectAtIndex:indexPath.row]).rcStation_relationship.name;
        //result.textLabel.text = @"SILETZ AT SILETZ";
        result.detailTextLabel.text =  (NSString*)[[RCModelShared PETypeDefinitions] objectForKey:((RCChartItem *)[self.filteredTableData objectAtIndex:indexPath.row]).peType];
    }
    else
    {
        //result.textLabel.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
        result.textLabel.text = ((RCChartItem *)[[RCModelShared chartItems] objectAtIndex:indexPath.row]).rcStation_relationship.name;
        result.detailTextLabel.text = (NSString*)[[RCModelShared PETypeDefinitions] objectForKey:((RCChartItem *)[[RCModelShared chartItems] objectAtIndex:indexPath.row]).peType];
    }
    return result;
    
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Navigation logic may go here. Create and push another view controller.
//    /*
//     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
//     // ...
//     // Pass the selected object to the new view controller.
//     [self.navigationController pushViewController:detailViewController animated:YES];
//     */
//}
-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString *)searchText
{
    if([searchText length]==0)
    {
        isFiltered = NO;
        //[self.searchBar resignFirstResponder];
    }
    else {
        isFiltered = YES;
        self.filteredTableData = [[NSMutableArray alloc] init];
        self.filteredDataConversionToFullData = [[NSMutableDictionary alloc] init];
        NSUInteger index = 0;
        NSUInteger filteredTableIndex =0;
        for(RCChartItem *chartItem in [RCModelShared chartItems])
        {
            NSRange nameRange = [chartItem.rcStation_relationship.name rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if(nameRange.location!=NSNotFound)
            {
                [self.filteredTableData addObject:chartItem];
                [self.filteredDataConversionToFullData setObject:[NSNumber numberWithUnsignedInteger:index] forKey:[NSNumber numberWithUnsignedInteger:filteredTableIndex]];
                filteredTableIndex++;
            }
            index++;
        }
    }
    [self.chartItemTableView reloadData];
    //NSLog(@"search text: %@",searchText);
}

-(void)searchBarSearchButtonClicked:(UISearchBar*)searchBar
{
    [searchBar resignFirstResponder];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.searchBar resignFirstResponder];
}

- (void)viewDidUnload {
    [self setSearchBar:nil];
    [self setChartItemTableView:nil];
    [super viewDidUnload];
}

//-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
//{
//    
//}

-(void )tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.searchBar resignFirstResponder];

//    //show activity spinner
//    self.HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//	self.HUD.delegate = (id)self;
    
    //RCTestOfPush *testOfPush =(RCTestOfPush *)[self.storyboard instantiateViewControllerWithIdentifier:@"TestOfPush"];
    //[[RCTestOfPush alloc] init];
    //    [self.navigationController pushViewController:testOfPush animated:YES];
    //return;
    RCScatterPlotViewController *scatterPlotViewController =(RCScatterPlotViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ScatterPlot"];
    
    //[[RCScatterPlotViewController alloc]init];
    //NSLog(@"selected row %d",[indexPath indexAtPosition:1]);

    
    int selectedRowIndex =indexPath.row;
    //[self.chartItemTableView indexPathForSelectedRow].row;
    RCChartItem *chartItem;
    if(self.isFiltered)
    {
        chartItem= (RCChartItem *)[self.filteredTableData objectAtIndex:selectedRowIndex];
    }
    else
    {
        chartItem= (RCChartItem *)[[RCModelShared chartItems] objectAtIndex:selectedRowIndex];
    }
    
    scatterPlotViewController.PEType = chartItem.peType;
    scatterPlotViewController.stationID = chartItem.stationID;
    scatterPlotViewController.itemName = chartItem.rcStation_relationship.name;
    scatterPlotViewController.hidesBottomBarWhenPushed = true;
    scatterPlotViewController.chartItem = chartItem;
    scatterPlotViewController.isLogPlot=NO;
    
    [self.navigationController pushViewController:scatterPlotViewController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    //return indexPath;
}
//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if([[segue identifier] isEqualToString:@"showChartForSelection"]){
//        RCScatterPlotViewController *scatterPlotViewController = [segue destinationViewController];
//        //scatterPlotViewController
//        
//        int selectedRowIndex =[self.chartItemTableView indexPathForSelectedRow].row;
//        RCChartItem *chartItem;
//        if(self.isFiltered)
//        {
//            chartItem= (RCChartItem *)[self.filteredTableData objectAtIndex:selectedRowIndex];
//        }
//        else
//        {
//            chartItem= (RCChartItem *)[[RCModelShared chartItems] objectAtIndex:selectedRowIndex];
//        }
//        
//        scatterPlotViewController.PEType = chartItem.PEType;
//        scatterPlotViewController.stationID = chartItem.stationID;
//        scatterPlotViewController.itemName = chartItem.name;
//        scatterPlotViewController.hidesBottomBarWhenPushed = true;
//    }
//    
//}

//- (void)hudWasHidden:(MBProgressHUD *)hud {
//	// Remove HUD from screen when the HUD was hidded
//	[_HUD removeFromSuperview];
//	//[HUD release];
//	_HUD = nil;
//}

@end
