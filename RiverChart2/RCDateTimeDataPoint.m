//
//  RCDateTimeDataPoint.m
//  RiverChart2
//
//  Created by Tim Fiez on 2/20/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCDateTimeDataPoint.h"

@implementation RCDateTimeDataPoint
@synthesize XValue=_XValue;
@synthesize YValue=_YValue;

-(id)initWithXValueAndYValue:(NSDate*)XValue YValue:(double)YValue
{
    if(self = [super init])
    {
        self.XValue = XValue;
        self.YValue = YValue;
    }
    return self;
}

@end
