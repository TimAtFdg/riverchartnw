//
//  RCConstants.h
//  RiverChart2
//
//  Created by Tim Fiez on 2/2/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const ObservedPlotConst;
extern NSString* const PredictedPlotConst;
extern NSString* const DividerPlotConst;
extern NSTimeInterval const oneDay;

static NSString *kChartDataFileName = @"%@_%@";
static NSString *kChartLogDataFileName = @"%@_%@_%@";

//static NSString *kGetChartDataUrl = @"http://50.188.138.220/RiverChartNWWebApplication/api/ChartData?stationID=%@&PEType=%@";
static NSString *kGetChartDataUrl = @"http://www.fiezdevelopmentgroup.com/applications/RiverChartNWWebApplication/api/ChartData?stationID=%@&PEType=%@";

// time interval for using saved chart data files
static const int kChartDataTimeInterval=-900;  // 15 minutes


//static NSString *_getChartDataUrl = @"http://50.188.138.220/RiverChartNWWebApplication/api/ChartData?stationID=%@&PEType=%@&logDate=%@";
//static NSString *_getChartDataUrl = @"http://50.188.138.220/RiverChartNWWebApplicationDev/api/ChartData?stationID=%@&PEType=%@&logDate=%@";
static NSString *kGetChartDataUrlForLog = @"http://www.fiezdevelopmentgroup.com/applications/RiverChartNWWebApplication/api/ChartData?stationID=%@&PEType=%@&logDate=%@";

//static NSString *_getAllChartItemsUrl = @"http://50.188.138.220/RiverChartWebApplication/api/DataItem";
static NSString *kGetAllChartItemsUrl = @"http://www.fiezdevelopmentgroup.com/applications/RiverChartNWWebApplication/api/DataItem";