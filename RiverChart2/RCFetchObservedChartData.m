//
//  RCFetchObservedChartData.m
//  RiverChart2
//
//  Created by Tim on 12/27/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCFetchObservedChartData.h"
#import "RCGlobalSingleton.h"
#import "RCConstants.h"
#define kTimeout 60.0

@implementation RCFetchObservedChartData
+(RCChartData*)GetForStation:(NSString*)stationID PEType:(NSString*)PEType LogDate:(NSDate*)logDate
{
    RCChartData* chartData;// = [[RCChartData alloc] init];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *logDateString = [dateFormat stringFromDate:logDate];
    
    NSString *chartDataDirectory = [NSString stringWithFormat:@"%@/ChartLogData",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]];
    // look to see if file exits and is less than 1 hour old
    NSString *filePath = [chartDataDirectory stringByAppendingPathComponent:[NSString stringWithFormat:kChartLogDataFileName,stationID,PEType,logDateString]];
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        NSError *error = nil;
        NSDictionary *chartDataDictionary = nil;
        NSData *chartNSData = [NSData dataWithContentsOfFile:filePath];
        
        if(chartNSData==nil) // try getting from server
        {
            chartData=[self GetForStationFromServer:stationID PEType:PEType LogDateString:logDateString];
        }
        else
        {
            chartDataDictionary = [NSJSONSerialization JSONObjectWithData:chartNSData options:NSJSONReadingAllowFragments error:&error];
            chartData = [[RCChartData alloc] initWithDictionary:chartDataDictionary];
        }

    }
    else if([[RCGlobalSingleton globalSingleton] networkStatus]!=NotReachable)
    {
        chartData=[self GetForStationFromServer:stationID PEType:PEType LogDateString:logDateString];
    }
    else
        chartData=nil;
    return chartData;
}

+(RCChartData*)GetForStationFromServer:(NSString*)stationID PEType:(NSString*)PEType LogDateString:(NSString*)logDateString
{
    RCChartData* chartData;// = [[RCChartData alloc] init];

    NSString* requestUrl = [NSString stringWithFormat:kGetChartDataUrlForLog,stationID,PEType,logDateString];
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:kTimeout];
    NSHTTPURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:req returningResponse:&response error:&error];
    if(data!=nil)
    {
        NSError *error = nil;
        NSDictionary *chartDataDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        chartData = [[RCChartData alloc] initWithDictionary:chartDataDictionary];
        [self saveChartLogData:data StationId:stationID PEType:PEType DateString:logDateString];
    }
    else
        chartData=nil;
    return chartData;
}

+(void)saveChartLogData:(NSData*)data StationId:(NSString*)stationID PEType:(NSString*)PEType DateString:(NSString*)dateString
{
    dispatch_queue_t saveCustomQueue;
    saveCustomQueue = dispatch_queue_create("com.fiezdevelopmentgroup.SaveLogDataCustomQueue", NULL);
    dispatch_async(saveCustomQueue, ^{
        NSString *chartDataDirectory = [NSString stringWithFormat:@"%@/ChartLogData",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]];
        BOOL isDir;
        if(![[NSFileManager defaultManager] fileExistsAtPath:chartDataDirectory isDirectory:&isDir])
        {
            if(![[NSFileManager defaultManager] createDirectoryAtPath:chartDataDirectory withIntermediateDirectories:YES attributes:nil error:nil])
                return;
        }
        NSString *filePath = [chartDataDirectory stringByAppendingPathComponent:[NSString stringWithFormat:kChartLogDataFileName,stationID,PEType,dateString]];
        [data writeToFile:filePath options:nil error:nil];
    });
}
@end