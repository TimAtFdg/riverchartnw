//
//  RCLogTableViewController.m
//  RiverChart2
//
//  Created by Tim Fiez on 5/28/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//
#define MAINLABEL_TAG 1
#define SECONDLABEL_TAG 2
#define RATE_TAG 3

#import "RCLogTableViewController.h"
#import "RCModelShared.h"
#import "RCLog.h"
#import "RCStation.h"
#import "RCGlobalSingleton.h"
#import "DYRateView.h"
#import "RCViewLogViewController.h"

@interface RCLogTableViewController ()
{
    NSDateFormatter *dateFormatter;
}
@end

@implementation RCLogTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[RCGlobalSingleton globalSingleton] setLogTableView:self.tableView];

    dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"EEEE MMMM d, yyyy"];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)  name:UIDeviceOrientationDidChangeNotification  object:nil];
}

//-(void)viewDidAppear:(BOOL)animated
//{
//    
//}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)orientationChanged:(NSNotification *)notification{
    [self.tableView reloadData];
    //    if(self.keyBoardIsVisible)
    //       [self textViewDidChange:nil];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    if([RCModelShared logItems] == nil)
        return 0;
    else
        return [[RCModelShared logItems] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //UILabel *mainLabel, *secondLabel;
    DYRateView *rateView;
    int rateViewWidth = 75;
    UITableViewCell *cell = nil;
    static NSString *TableViewCellIdentifier = @"LogItemCell";
    cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
    //int mainLabelHeight = cell.bounds.size.height * 0.60;

    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:TableViewCellIdentifier];
        int mainLabelHeight = cell.bounds.size.height * 0.60;
        
        cell.textLabel.font = [UIFont systemFontOfSize:14.0];
//        int secondLabelHeight = cell.bounds.size.height - mainLabelHeight;
//        //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
//        
//        mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.bounds.size.width, mainLabelHeight)];
//        mainLabel.tag = MAINLABEL_TAG;
//        mainLabel.font = [UIFont systemFontOfSize:14.0];
//        mainLabel.textAlignment = UITextAlignmentLeft;
//        mainLabel.textColor = [UIColor blackColor];
//        mainLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
//        [cell.contentView addSubview:mainLabel];
//        
//        secondLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, mainLabelHeight, self.view.bounds.size.width - rateViewWidth, secondLabelHeight)];
//        secondLabel.tag = SECONDLABEL_TAG;
//        secondLabel.font = [UIFont systemFontOfSize:12.0];
//        secondLabel.textAlignment = UITextAlignmentRight;
//        secondLabel.textColor = [UIColor darkGrayColor];
//        secondLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
//        [cell.contentView addSubview:secondLabel];
        
        rateView = [[DYRateView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width - rateViewWidth-12, mainLabelHeight, rateViewWidth, (cell.bounds.size.height-mainLabelHeight)) fullStar:[UIImage imageNamed:@"StarFull.png"] emptyStar:[UIImage imageNamed:@"StarEmpty.png"]];
        rateView.tag = RATE_TAG;
        rateView.rate = 0.0;
        rateView.padding = 0.0;
        rateView.alignment = RateViewAlignmentCenter;
        rateView.editable = NO;
        //rateView.delegate = self;
        [cell.contentView addSubview:rateView];
    }
    else
    {
//        mainLabel = (UILabel *)[cell.contentView viewWithTag:MAINLABEL_TAG];
//        secondLabel = (UILabel *)[cell.contentView viewWithTag:SECONDLABEL_TAG];
        int mainLabelHeight = cell.bounds.size.height * 0.60;

        rateView = (DYRateView *)[cell.contentView viewWithTag:RATE_TAG];
        [[cell.contentView viewWithTag:RATE_TAG] removeFromSuperview];
        
        rateView = [[DYRateView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width - rateViewWidth-25, mainLabelHeight, rateViewWidth, (cell.bounds.size.height-mainLabelHeight)) fullStar:[UIImage imageNamed:@"StarFull.png"] emptyStar:[UIImage imageNamed:@"StarEmpty.png"]];
        rateView.tag = RATE_TAG;
        rateView.rate = 0.0;
        rateView.padding = 0.0;
        rateView.alignment = RateViewAlignmentCenter;
        rateView.editable = NO;
        //rateView.delegate = self;
        [cell.contentView addSubview:rateView];

        
    }
    //mainLabel.text = [((RCLog *)[[RCModelShared logItems] objectAtIndex:indexPath.row]).rcStation_relationship name];
    cell.textLabel.text = [((RCLog *)[[RCModelShared logItems] objectAtIndex:indexPath.row]).rcStation_relationship name];
    //NSDate* tempDate = ((RCLog *)[[RCModelShared logItems] objectAtIndex:indexPath.row]).date;
    //secondLabel.text = [dateFormatter stringFromDate:((RCLog *)[[RCModelShared logItems] objectAtIndex:indexPath.row]).date];
    cell.detailTextLabel.text = [dateFormatter stringFromDate:((RCLog *)[[RCModelShared logItems] objectAtIndex:indexPath.row]).date];
    rateView.rate = ((RCLog *)[[RCModelShared logItems] objectAtIndex:indexPath.row]).rating.floatValue;
    cell.textLabel.font = [UIFont systemFontOfSize:14.0];
    //result.textLabel.font = [UIFont systemFontOfSize:14.0];
    
    //        if(isFiltered)
    //        {
    //            result.textLabel.text =  ((RCChartItem *)[self.filteredTableData objectAtIndex:indexPath.row]).name;
    //            //result.textLabel.text = @"SILETZ AT SILETZ";
    //            result.detailTextLabel.text =  (NSString*)[[RCModelShared PETypeDefinitions] objectForKey:((RCChartItem *)[self.filteredTableData objectAtIndex:indexPath.row]).peType];
    //        }
    //        else
    //        {
    //result.textLabel.text = [NSStrinringWithFormat:@"%ld", (long)indexPath.row];
    //RCStation* tempStation = ((RCChartItem *)[[RCModelShared favoriteChartItems] objectAtIndex:indexPath.row]).rcStation_relationship;


    //result.textLabel.text = [((RCLog *)[[RCModelShared logItems] objectAtIndex:indexPath.row]).rcStation_relationship name];
                             //rcStation_relationship name];
    //result.detailTextLabel.text = (NSString*)[[RCModelShared PETypeDefinitions] objectForKey:((RCChartItem *)[[RCModelShared chartItems] objectAtIndex:indexPath.row]).peType];
    //}
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
   //     [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
                
            RCLog *logItem= (RCLog *)[[RCModelShared logItems] objectAtIndex:indexPath.row];
            
            // Delete the row from the data source
            // [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            //if(!chartItem.isFavorite)
            //{
            //chartItem.isFavorite = NO;
            //[RCModelShared updateFavorites];
        [RCModelShared deleteLogItem:logItem];
            [self.tableView reloadData];
            
            
            //}
      //  }
    
//    else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"showLogForSelection"]){
        RCViewLogViewController *viewLogViewController = [segue destinationViewController];
        //scatterPlotViewController
        
        int selectedRowIndex =[self.tableView indexPathForSelectedRow].row;
        
        viewLogViewController.logItem = [[RCModelShared logItems] objectAtIndex:selectedRowIndex];
        viewLogViewController.hidesBottomBarWhenPushed=true;
    }
    
}


//#pragma mark - Table view delegate

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    int selectedRowIndex =indexPath.row;
//    
//    RCViewLogViewController *logViewController =(RCViewLogViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ViewLog"];
//    logViewController.logItem = [[RCModelShared logItems] objectAtIndex:selectedRowIndex];
//    //rateItViewController.chartItem = self.chartItem;
//    
//    [self.navigationController presentViewController:logViewController animated:YES completion:NULL];
//    
//    
//    // Navigation logic may go here. Create and push another view controller.
//    /*
//     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
//     // ...
//     // Pass the selected object to the new view controller.
//     [self.navigationController pushViewController:detailViewController animated:YES];
//     */
//}

@end
