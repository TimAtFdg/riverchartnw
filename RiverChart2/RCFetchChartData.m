//
//  RCFetchChartData.m
//  RiverChart2
//
//  Created by Tim Fiez on 2/13/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCFetchChartData.h"
#import "RCConstants.h"
#import "RCGlobalSingleton.h"

#define kTimeout 60.0

@interface RCFetchChartData()
+(RCChartData*)GetForStationFromServer:(NSString*)stationID PEType:(NSString*)PEType;
+(void)saveChartData:(NSData*)data StationId:(NSString*)stationID PEType:(NSString*)PEType;
@end

@implementation RCFetchChartData
+(RCChartData*)GetForStation:(NSString*)stationID PEType:(NSString*)PEType
{
    
    RCChartData* chartData;// = [[RCChartData alloc] init];
    //NSString *chartDataDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/ChartData"];
    NSString *chartDataDirectory = [NSString stringWithFormat:@"%@/ChartData",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]];
    // look to see if file exits and is less than 1 hour old
    NSString *filePath = [chartDataDirectory stringByAppendingPathComponent:[NSString stringWithFormat:kChartDataFileName,stationID,PEType]];
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
        NSDate *modificationDate = [attributes fileModificationDate];
        if([modificationDate timeIntervalSinceNow] < kChartDataTimeInterval && [[RCGlobalSingleton globalSingleton] networkStatus]!=NotReachable) // get new file
        {
            chartData=[self GetForStationFromServer:stationID PEType:PEType];
        }

        else // return saved file
        {
            NSError *error = nil;
            NSDictionary *chartDataDictionary = nil;
            NSData *chartNSData = [NSData dataWithContentsOfFile:filePath];
            
            if(chartNSData==nil) // try getting from server
            {
                chartData=[self GetForStationFromServer:stationID PEType:PEType];
            }
            else
            {
                chartDataDictionary = [NSJSONSerialization JSONObjectWithData:chartNSData options:NSJSONReadingAllowFragments error:&error];
                chartData = [[RCChartData alloc] initWithDictionary:chartDataDictionary];
            }
        }
    }
    else
    {
        if([[RCGlobalSingleton globalSingleton] networkStatus]==NotReachable)
            chartData = nil;
        else
            chartData=[self GetForStationFromServer:stationID PEType:PEType];
    }
    return chartData;
}

+(RCChartData*)GetForStationFromServer:(NSString*)stationID PEType:(NSString*)PEType
{
    RCChartData* chartData;
    NSString* requestUrl = [NSString stringWithFormat:kGetChartDataUrl,stationID,PEType];
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:kTimeout];
    NSHTTPURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:req returningResponse:&response error:&error];
    if(data!=nil)
    {
        //_chartItems = [[NSMutableArray alloc] init];
        NSError *error = nil;
        //NSArray *chartDataResults = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        NSDictionary *chartDataDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        chartData = [[RCChartData alloc] initWithDictionary:chartDataDictionary];
       // NSDictionary *chartDataResult = chartDataResults;
       // int x = 5;
        //NSArray *results = [searchResults objectForKey:@"results"];
      //  for(NSDictionary *searchResult in searchResults)
            //NSDictionary *searchResult = searchResults[0];
           // [_chartItems addObject:[[RCChartItem alloc] initWithDictionary:searchResult]];
        [self saveChartData:data StationId:stationID PEType:PEType];
    }
    return chartData;
}

+(void)saveChartData:(NSData*)data StationId:(NSString*)stationID PEType:(NSString*)PEType
{
    dispatch_queue_t saveCustomQueue;
    saveCustomQueue = dispatch_queue_create("com.fiezdevelopmentgroup.SaveDataCustomQueue", NULL);
    dispatch_async(saveCustomQueue, ^{
        //NSString *chartDataDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/ChartData"];
        NSString *chartDataDirectory = [NSString stringWithFormat:@"%@/ChartData",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]];
        BOOL isDir;
        if(![[NSFileManager defaultManager] fileExistsAtPath:chartDataDirectory isDirectory:&isDir])
        {
            if(![[NSFileManager defaultManager] createDirectoryAtPath:chartDataDirectory withIntermediateDirectories:YES attributes:nil error:nil])
                return;
        }
        NSString *filePath = [chartDataDirectory stringByAppendingPathComponent:[NSString stringWithFormat:kChartDataFileName,stationID,PEType]];
        NSError *error;
        [data writeToFile:filePath options:nil error:&error];
        if(error)
            NSLog(@"error: %@", error.description);
    });
}
@end
