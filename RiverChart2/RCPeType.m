//
//  RCPeType.m
//  RiverChart2
//
//  Created by Tim Fiez on 5/22/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCPeType.h"


@implementation RCPeType

@dynamic stationID;
@dynamic peType;
@dynamic isFavorite;
@dynamic rcStation_relationship;

@end
