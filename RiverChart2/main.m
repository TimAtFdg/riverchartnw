//
//  main.m
//  RiverChart2
//
//  Created by Tim Fiez on 12/28/12.
//  Copyright (c) 2012 Tim Fiez. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RCAppDelegate class]));
    }
}
