//
//  RCConstants.m
//  RiverChart2
//
//  Created by Tim Fiez on 2/2/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCConstants.h"

NSString* const ObservedPlotConst = @"ObservedPlot";
NSString* const PredictedPlotConst = @"PredictedPlot";
NSString* const DividerPlotConst = @"DividerPlot";
NSTimeInterval const oneDay = 60*60*24;