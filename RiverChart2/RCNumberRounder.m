//
//  RCNumberRounder.m
//  RiverChart2
//
//  Created by Tim Fiez on 4/1/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCNumberRounder.h"

@implementation RCNumberRounder
+(double)RoundUpTenth:(double)number
{
    return ceil(number*10.0)/10.0;
}
+(double)RoundDownTenth:(double)number
{
    return floor(number*10.0)/10.0;
}
+(double)RoundUp:(double)number
{
    return ceil(number);
}
+(double)RoundDown:(double)number
{
    return floor(number);
}
+(double)RoundUpTens:(double)number
{
    return ceil(number/10.0)*10.0;
}
+(double)RoundDownTens:(double)number
{
    return floor(number/10.0)*10.0;
}
+(double)RoundUpHundreds:(double)number
{
    return ceil(number/100.0)*100.0;
}
+(double)RoundDownHundreds:(double)number
{
    return floor(number/100.0)*100.0;
}
@end
