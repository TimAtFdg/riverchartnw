//
//  RCSearchTableViewController.h
//  RiverChart2
//
//  Created by Tim Fiez on 1/22/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCFavoritesTableViewController : UITableViewController
//@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) IBOutlet UITableView *chartItemTableView;
@end
