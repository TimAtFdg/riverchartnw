//
//  RCLog.m
//  RiverChart2
//
//  Created by Tim on 12/20/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCLog.h"
#import "RCStation.h"


@implementation RCLog

@dynamic date;
@dynamic note;
@dynamic rating;
@dynamic stationID;
@dynamic peType;
@dynamic rcStation_relationship;

@end
