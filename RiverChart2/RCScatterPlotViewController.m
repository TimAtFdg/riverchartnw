//
//  RCScatterPlotViewController.m
//  RiverChart2
//
//  Created by Tim Fiez on 1/26/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCScatterPlotViewController.h"
#import "RCFetchChartData.h"
#import "RCConstants.h"
//#import "RCDataPoint.h"
#import "RCDateTimeDataPoint.h"
#import "RCNumberRounder.h"
#import "RCModelShared.h"
#import "RCGlobalSingleton.h"
#import "RCAddLogViewController.h"
#import "RCFetchObservedChartData.h"
#import "RCModelShared.h"

typedef enum { Unit1, Unit2 } MapUnit;

@interface RCScatterPlotViewController ()
@property RCChartData *chartData;
@property MapUnit selectedMapUnit;
@property BOOL hasUnit1Data;
@property BOOL hasUnit2Data;
@property double chartMaxDataValue;
@property double chartMinDataValue;
@property NSNumberFormatter *yAxisNumberFormatter;
@property double yAxisGridInterval;
@property UIImage *btnImageUnit1;
@property UIImage *btnImageUnit2;
@property UIBarButtonItem *addBookmarkButton;
@end

@implementation RCScatterPlotViewController

@synthesize hostView = _hostView;
@synthesize stationID;
@synthesize PEType;
@synthesize itemName;
@synthesize chartData = _chartData;
@synthesize selectedMapUnit = _selectedMapUnit;
@synthesize hasUnit1Data = _hasUnit1Data;
@synthesize hasUnit2Data = _hasUnit2Data;
//@synthesize favoriteBarButtonItem;
//@synthesize unit1BarButtonItem;
//@synthesize unit2BarButtonItem;
//@synthesize rateItBarButtonItem;
//@synthesize toolbar;
@synthesize plotLabel;
@synthesize errorView;
@synthesize chartMaxDataValue;
@synthesize chartMinDataValue;
@synthesize yAxisNumberFormatter;
@synthesize yAxisGridInterval;
@synthesize chartItem;

//@synthesize navigationBar = _navigationBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //show activity spinner
    self.HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //self.HUD.labelText = @"Authenticating";
	self.HUD.delegate = (id)self;
    
    self.hostView.allowPinchScaling=NO;
    
    if(!self.isLogPlot)
    {
        UIImage *image;
        if(chartItem.isFavorite)
            image= [UIImage imageNamed:@"726-star-selected.png"];
        else
            image = [UIImage imageNamed:@"726-star.png"];
        
        _addBookmarkButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(bookmarkAction:)];
        
        [self.navigationItem setRightBarButtonItem:_addBookmarkButton];
    }
    else
        [self.logItButton setHidden:YES];
    
    self.navigationItem.title=self.itemName;
    //self.plotLabel.text = self.itemName;
    
    [self.hostView setHidden:YES];
    [self.errorView setHidden:YES];
    self.unitSwitchButton.hidden=YES;

}
-(void)getPlotData
{
    if(self.isLogPlot)
        self.chartData = [RCFetchObservedChartData GetForStation:self.stationID PEType:self.PEType LogDate:self.logDate];
    else
        self.chartData = [RCFetchChartData GetForStation:self.stationID PEType:self.PEType];
    
    if([self.chartData ObservedUnit1Data] !=nil && [[self.chartData ObservedUnit1Data] count]>0) // have unit1 data
        self.hasUnit1Data = YES;
    else
        self.hasUnit1Data = NO;
    if([self.chartData ObservedUnit2Data] !=nil && [[self.chartData ObservedUnit2Data] count]>0) // have unit2 data
        self.hasUnit2Data = YES;
    else
        self.hasUnit2Data = NO;

    if(self.hasUnit1Data == NO && self.hasUnit2Data == NO)
        self.chartData.DataRetrievedSuccessfully = NO;
    
    if(self.chartData.DataRetrievedSuccessfully == NO)
    {
        [self.hostView setHidden:YES];
        [self.errorView setHidden:NO];
    }
    else
    {
        [self.errorView setHidden:YES];
        [self.hostView setHidden:NO];

        if(self.hasUnit1Data && self.hasUnit2Data)
        {
            self.btnImageUnit2 = [UIImage imageNamed:@"downBlue.png"]; // will default to unit2 being the first unit shown
            self.btnImageUnit1 = [UIImage imageNamed:@"upBlue.png"];

            self.unitSwitchButton.enabled=YES;
            self.unitSwitchButton.hidden=YES;
            if([RCModelShared lastSelectedUnit]== Unit1)
            {
                self.selectedMapUnit = Unit1;
                self.plotLabel.text = self.chartData.YAxisLabelUnit1;
                [self.unitSwitchButton setImage:self.btnImageUnit1 forState:UIControlStateNormal];
            }
            else
            {
                self.selectedMapUnit = Unit2;
                self.plotLabel.text = self.chartData.YAxisLabelUnit2;
                [self.unitSwitchButton setImage:self.btnImageUnit2 forState:UIControlStateNormal];
            }
        }
        else
        {
            self.unitSwitchButton.hidden=YES;
            self.unitSwitchButton.enabled=NO;
            if(self.hasUnit1Data)
            {
                self.plotLabel.text = self.chartData.YAxisLabelUnit1;
                self.selectedMapUnit = Unit1;
            }
            else if(self.hasUnit2Data)
            {
                self.plotLabel.text = self.chartData.YAxisLabelUnit1;
                self.selectedMapUnit = Unit2;
            }
            else // this should be some sort of error situation
                self.plotLabel.text = self.chartData.YAxisLabelUnit1;
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)  name:UIDeviceOrientationDidChangeNotification  object:nil];
}


-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self getPlotData];
    [self setUnitLabelWidthAndPositionUnitSwitch];
    if(self.chartData.DataRetrievedSuccessfully)
        [self initPlot];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Rotation
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return true;// (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (void)orientationChanged:(NSNotification *)notification{
//    if(((UIDevice*)notification.object).orientation==UIDeviceOrientationPortrait)
//        NSLog(@"portrait");
//    else
//        NSLog(@"not portrait");
    [self setUnitLabelWidthAndPositionUnitSwitch];
}

#pragma mark uisetup
-(void)setUnitLabelWidthAndPositionUnitSwitch
{
//    if(self.plotLabel.hidden)
//        self.plotLabel.hidden=NO;
    CGSize size=[self.plotLabel.text sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]}];
    //UIInterfaceOrientation orientation=[UIApplication sharedApplication].statusBarOrientation;
    CGFloat viewWidth=self.view.frame.size.width;
//    if(orientation==UIInterfaceOrientationPortrait && self.view.frame.size.width > self.view.frame.size.height)
//        viewWidth = self.view.frame.size.height;
//    if((orientation==UIInterfaceOrientationLandscapeLeft || orientation==UIInterfaceOrientationLandscapeRight) && self.view.frame.size.height>self.view.frame.size.width)
//        viewWidth = self.view.frame.size.height;
    CGRect newLabelRect = CGRectMake(floor(viewWidth/2.0-size.width/2.0), 0, size.width+1, self.plotLabel.frame.size.height);
    self.plotLabel.frame = newLabelRect;
    if(self.unitSwitchButton.enabled)
    {
//        if(self.unitSwitchButton.hidden)
//            self.unitSwitchButton.hidden=NO;
        CGRect unitSwitchRect = self.unitSwitchButton.frame;
        CGRect newUnitSwitchRect = CGRectMake(newLabelRect.origin.x+newLabelRect.size.width+1, 0, unitSwitchRect.size.width, unitSwitchRect.size.height);
        self.unitSwitchButton.frame = newUnitSwitchRect;
        if(self.unitSwitchButton.hidden)
            self.unitSwitchButton.hidden=NO;
    }

    
}

#pragma mark - CPTPlotDataSource methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    if([plot.identifier isEqual:PredictedPlotConst])
    {
        if(self.selectedMapUnit == Unit1)
            return [[self.chartData ForecastUnit1Data] count];
        else
            return [[self.chartData ForecastUnit2Data] count];
    }
    else if([plot.identifier isEqual:ObservedPlotConst])
    {
        if(self.selectedMapUnit == Unit1)
            return [[self.chartData ObservedUnit1Data] count];
        else
            return [[self.chartData ObservedUnit2Data] count];
    }
    else if([plot.identifier isEqual:DividerPlotConst])
        return 2;
    else // this is some sort of error condition
        return 0;
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    //return [NSDecimalNumber zero];
    
    switch (fieldEnum)
    {
        case CPTScatterPlotFieldX:
            //return [NSNumber numberWithDouble:(index*10.0)];
            if ([plot.identifier isEqual:PredictedPlotConst] == YES)
            {
                //return [NSNumber numberWithInt:index];
                
                if(self.selectedMapUnit == Unit1)
                {
                    RCDateTimeDataPoint *dataPoint = [[self.chartData ForecastUnit1Data] objectAtIndex:index ];
                    //double temp1 = [dataPoint.XValue timeIntervalSinceReferenceDate];
                    //return [NSNumber numberWithDouble:dataPoint.XValue];
                    return [NSNumber numberWithDouble:[dataPoint.XValue timeIntervalSinceReferenceDate]];
                    
                }
                else // is unit2
                {
                    RCDateTimeDataPoint *dataPoint = [[self.chartData ForecastUnit2Data] objectAtIndex:index ];
                    return [NSNumber numberWithDouble:[dataPoint.XValue timeIntervalSinceReferenceDate]];
                }
            }
            else if ([plot.identifier isEqual:ObservedPlotConst] == YES)
            {
                //return [NSNumber numberWithInt:index];
                
                if(self.selectedMapUnit == Unit1)
                {
                    RCDateTimeDataPoint *dataPoint = [[self.chartData ObservedUnit1Data] objectAtIndex:index ];
                    //double temp1 = dataPoint.XValue;
                    return [NSNumber numberWithDouble:[dataPoint.XValue timeIntervalSinceReferenceDate]];
                    //return [NSNumber numberWithDouble:(dataPoint.XValue - 1000000000000000)];
                    //return [NSNumber numberWithInt:index];
                    
                }
                else // is unit2
                {
                    RCDateTimeDataPoint *dataPoint = [[self.chartData ObservedUnit2Data] objectAtIndex:index ];
                    return [NSNumber numberWithDouble:[dataPoint.XValue timeIntervalSinceReferenceDate]];
                }
            }
            else if ([plot.identifier isEqual:DividerPlotConst] == YES)
            {
                if(self.isLogPlot)
                {
                    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar ];
                    [calendar setTimeZone:[[NSTimeZone alloc] initWithName:@"UTC"]];
                    
                    NSDateComponents *components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:self.logDate];
                    [components setHour:12];
                    [components setMinute:0];
                    [components setSecond:0];
                    
                    return [NSNumber numberWithDouble:[[calendar dateFromComponents:components] timeIntervalSinceReferenceDate]];
                }
                else
                {
                    if(self.selectedMapUnit == Unit1)
                    {
                        
                        RCDateTimeDataPoint *dataPoint = [[self.chartData ObservedUnit1Data] objectAtIndex:(self.chartData.ObservedUnit1Data.count-1)];
                        return [NSNumber numberWithDouble:[dataPoint.XValue timeIntervalSinceReferenceDate]];
                    }
                    else // is unit2
                    {
                        RCDateTimeDataPoint *dataPoint = [[self.chartData ObservedUnit2Data] objectAtIndex:(self.chartData.ObservedUnit2Data.count-1)];
                        return [NSNumber numberWithDouble:[dataPoint.XValue timeIntervalSinceReferenceDate]];
                    }
                }
            }
            break;
        case CPTScatterPlotFieldY:
            if ([plot.identifier isEqual:PredictedPlotConst] == YES)
            {
                //return [NSNumber numberWithInt:index];
                
                if(self.selectedMapUnit == Unit1)
                {
                    RCDateTimeDataPoint *dataPoint = [[self.chartData ForecastUnit1Data] objectAtIndex:index ];
                    return [NSNumber numberWithDouble:dataPoint.YValue];
                }
                else // is unit2
                {
                    RCDateTimeDataPoint *dataPoint = [[self.chartData ForecastUnit2Data] objectAtIndex:index ];
                    return [NSNumber numberWithDouble:dataPoint.YValue];
                }
                //return [(RCDataPoint*)[[self.chartData ForecastUnit1Data] objectAtIndex:index];
            }
            else if ([plot.identifier isEqual:ObservedPlotConst] == YES)
            {
                // return [NSNumber numberWithInt:(index+5)];
                if(self.selectedMapUnit == Unit1)
                {
                    RCDateTimeDataPoint *dataPoint = [[self.chartData ObservedUnit1Data] objectAtIndex:index ];
                    return [NSNumber numberWithDouble:dataPoint.YValue];
                }
                else // is unit2
                {
                    RCDateTimeDataPoint *dataPoint = [[self.chartData ObservedUnit2Data] objectAtIndex:index ];
                    return [NSNumber numberWithDouble:dataPoint.YValue];
                }
            }
            else if ([plot.identifier isEqual:DividerPlotConst] == YES)
            {
                CPTGraph *graph = self.hostView.hostedGraph;
                CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
                
                if(index==0)
                    return [NSNumber numberWithDouble:plotSpace.yRange.locationDouble];
                else
                    return [NSNumber numberWithDouble:plotSpace.yRange.endDouble];
            }
            break;
    }
    return nil;
}

- (void)viewDidUnload {
    [self setHostView:nil];
  //  [self setNavigationBar:nil];
//    [self setToolbar:nil];
//    [self setUnit1BarButtonItem:nil];
//    [self setUnit2BarButtonItem:nil];
    //[self setFavoriteBarButtonItem:nil];
    //[self setFavoriteBarButtonItem:nil];
    [self setPlotLabel:nil];
    [self setErrorView:nil];
//    [self setRateItBarButtonItem:nil];
    [super viewDidUnload];
}

#pragma mark - Chart behavior
-(void)initPlot {
    //[self configureHost];
    [self computeYAxisParameters];
    [self configureGraph];
    [self configurePlots];
    [self configureAxes];
}

//-(void)configureHost {
//    // 1 - Set up view frame
////    CGRect parentRect = self.view.bounds;
////    CGSize toolbarSize = CGSizeMake(100.0, 100.0);//  self.toolbar.bounds.size;
////    parentRect = CGRectMake(parentRect.origin.x, (parentRect.origin.y + toolbarSize.height), parentRect.size.width,(parentRect.size.height - toolbarSize.height));
//     //   parentRect = CGRectMake(parentRect.origin.x, (parentRect.origin.y + 50), parentRect.size.width,(parentRect.size.height - 50));
//    // 2 - Create host view
//    //self.hostView = [(CPTGraphHostingView *) [CPTGraphHostingView alloc] initWithFrame:parentRect];
//    //self.hostView.allowPinchScaling = YES;
//    //[self.view addSubview:self.hostView];
//}

-(void)configureGraph {
    // 1 - Create the graph
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:self.hostView.frame];
    //graph = [(CPTXYGraph *)[CPTXYGraph alloc] initWithFrame:CGRectZero];
    //CPTGraph *graph = [(CPTXYGraph *)[CPTXYGraph alloc] initWithFrame:CGRectZero];
    self.hostView.hostedGraph = graph;
    
    graph.paddingRight= 0.0f;
    graph.paddingLeft = 0.0f;
    graph.paddingTop = 0.0f;
    graph.paddingBottom = 0.0f;
    
    //[graph applyTheme:[CPTTheme themeNamed:kCPTDarkGradientTheme]];
    //self.hostView.hostedGraph = graph;
    // 2 - Set graph title
    //NSString *title = self.itemName;
    //graph.title = title;
    // 3 - Create and set text style
    //CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
    //titleStyle.color = [CPTColor blackColor];
   // titleStyle.fontName = @"Helvetica-Bold";
    //titleStyle.fontSize = 8.0f;
    //graph.titleTextStyle = titleStyle;
    //graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    //graph.titleDisplacement = CGPointMake(0.0f, 10.0f);
    // 4 - Set padding for plot area
    [graph.plotAreaFrame setPaddingLeft:50.0f];
    [graph.plotAreaFrame setPaddingBottom:30.0f];
    [graph.plotAreaFrame setPaddingRight:20.f];
    
    if(self.isLogPlot)
        [graph.plotAreaFrame setPaddingTop:22.f];
    else
        [graph.plotAreaFrame setPaddingTop:18.f];
    
    // set border
    CPTMutableLineStyle *borderLineStyle = [CPTLineStyle lineStyle];
    borderLineStyle.lineColor = [CPTColor blackColor];
    borderLineStyle.lineWidth = 1.0f;
    //graph.plotAreaFrame.borderLineStyle = borderLineStyle;
    graph.plotAreaFrame.plotArea.borderLineStyle = borderLineStyle;
    // 5 - Enable user interactions for plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    plotSpace.allowsUserInteraction = NO;
}

-(void)configurePlots {
    // 1 - Get graph and plot space
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    
    CPTScatterPlot *observedPlot = [[CPTScatterPlot alloc] init];
    observedPlot.dataSource = self;
    observedPlot.identifier = ObservedPlotConst;
    CPTColor *observedPlotColor =[CPTColor colorWithComponentRed:0.0 green:0.478 blue:1.0 alpha:1.0]; //  [CPTColor greenColor];
    [graph addPlot:observedPlot toPlotSpace:plotSpace];
    
    // add plot for forecast values if they exist
    if((self.selectedMapUnit == Unit1 && self.chartData.ForecastUnit1Data !=nil && self.chartData.ForecastUnit1Data.count>0) || (self.selectedMapUnit == Unit2 && self.chartData.ForecastUnit2Data !=nil && self.chartData.ForecastUnit2Data.count>0)) {

        CPTScatterPlot *predictedPlot = [[CPTScatterPlot alloc] init];
        predictedPlot.dataSource = self;
        predictedPlot.identifier = PredictedPlotConst;
        CPTColor *predictedPlotColor = [CPTColor colorWithComponentRed:0.498 green:0.89 blue:0.98 alpha:1.0]; //[CPTColor blueColor];
        [graph addPlot:predictedPlot toPlotSpace:plotSpace];
        
        CPTMutableLineStyle *predictedLineStyle = [predictedPlot.dataLineStyle mutableCopy];
        predictedLineStyle.lineWidth = 3.0;
        predictedLineStyle.lineColor = predictedPlotColor;
        predictedPlot.dataLineStyle = predictedLineStyle;
        
        // add divider plot
        CPTScatterPlot *dividerPlot = [[CPTScatterPlot alloc] init];
        dividerPlot.dataSource = self;
        dividerPlot.identifier = DividerPlotConst;
        [graph addPlot:dividerPlot toPlotSpace:plotSpace];
        CPTMutableLineStyle *dividerLineStyle = [predictedPlot.dataLineStyle mutableCopy];
        dividerLineStyle.lineWidth = 1.0;
        dividerLineStyle.lineColor = [CPTColor blackColor];
        dividerPlot.dataLineStyle = dividerLineStyle;
    }
    else if(self.isLogPlot) // add divider to show date
    {
        // add divider plot
        CPTScatterPlot *dividerPlot = [[CPTScatterPlot alloc] init];
        dividerPlot.dataSource = self;
        dividerPlot.identifier = DividerPlotConst;
        [graph addPlot:dividerPlot toPlotSpace:plotSpace];
        CPTMutableLineStyle *dividerLineStyle = [dividerPlot.dataLineStyle mutableCopy];
        dividerLineStyle.lineWidth = 1.0;
        dividerLineStyle.lineColor = [CPTColor blackColor];
        dividerPlot.dataLineStyle = dividerLineStyle;
    }
    
    if(self.selectedMapUnit == Unit1)
    {
        NSTimeInterval minTime = [self computeGraphMinTime:self.chartData.minUnit1XValue];
        NSTimeInterval maxTime = [self computeGraphMaxTime:self.chartData.maxUnit1XValue];
        
        plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(minTime) length:CPTDecimalFromFloat(maxTime - minTime)];


        //plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromDouble(self.chartData.minUnit1YValue) length:CPTDecimalFromDouble(self.chartData.maxUnit1YValue - self.chartData.minUnit1YValue)];
    }
    else // unit2
    {
        NSTimeInterval minTime = [self computeGraphMinTime:self.chartData.minUnit2XValue];
        NSTimeInterval maxTime = [self computeGraphMaxTime:self.chartData.maxUnit2XValue];
        plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(minTime) length:CPTDecimalFromFloat(maxTime - minTime)];

//        plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(self.chartData.minUnit2YValue) length:CPTDecimalFromFloat(self.chartData.maxUnit2YValue - self.chartData.minUnit2YValue)];
    }
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromDouble(self.chartMinDataValue) length:CPTDecimalFromDouble(self.chartMaxDataValue - self.chartMinDataValue)];

    
    // 4 - Create styles and symbols
    CPTMutableLineStyle *observedLineStyle = [observedPlot.dataLineStyle mutableCopy];
    observedLineStyle.lineWidth = 3.0;
    observedLineStyle.lineColor = observedPlotColor;
    observedPlot.dataLineStyle = observedLineStyle;
    //CPTMutableLineStyle *observedSymbolLineStyle = [CPTMutableLineStyle lineStyle];
    //observedSymbolLineStyle.lineColor = observedPlotColor;
    //CPTPlotSymbol *observedSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    //observedSymbol.fill = [CPTFill fillWithColor:observedPlotColor];
    //observedSymbol.lineStyle = observedSymbolLineStyle;
    //observedSymbol.size = CGSizeMake(6.0f, 6.0f);
    //observedPlot.plotSymbol = observedSymbol;
    
    //CPTMutableLineStyle *predictedSymbolLineStyle = [CPTMutableLineStyle lineStyle];
    //predictedSymbolLineStyle.lineColor = predictedPlotColor;
    //CPTPlotSymbol *predictedSymbol = [CPTPlotSymbol starPlotSymbol];
    //predictedSymbol.fill = [CPTFill fillWithColor:predictedPlotColor];
    //predictedSymbol.lineStyle = predictedSymbolLineStyle;
    //predictedSymbol.size = CGSizeMake(6.0f, 6.0f);
    //predictedPlot.plotSymbol = predictedSymbol;
//    CPTMutableLineStyle *msftLineStyle = [msftPlot.dataLineStyle mutableCopy];
//    msftLineStyle.lineWidth = 2.0;
//    msftLineStyle.lineColor = msftColor;
//    msftPlot.dataLineStyle = msftLineStyle;
//    CPTMutableLineStyle *msftSymbolLineStyle = [CPTMutableLineStyle lineStyle];
//    msftSymbolLineStyle.lineColor = msftColor;
//    CPTPlotSymbol *msftSymbol = [CPTPlotSymbol diamondPlotSymbol];
//    msftSymbol.fill = [CPTFill fillWithColor:msftColor];
//    msftSymbol.lineStyle = msftSymbolLineStyle;
//    msftSymbol.size = CGSizeMake(6.0f, 6.0f);
//    msftPlot.plotSymbol = msftSymbol;
}

-(void)configureAxes {
    CPTGraph *graph = self.hostView.hostedGraph;
    
    CPTMutableTextStyle *axisTextStyle = [CPTTextStyle textStyle];
    CPTMutableLineStyle *axisStyle = [CPTLineStyle lineStyle];
    axisStyle.lineColor = [CPTColor blackColor];
    axisStyle.lineWidth = 1.0f;
    
    CPTMutableLineStyle *gridStyle = [CPTLineStyle lineStyle];
    gridStyle.lineColor = [CPTColor lightGrayColor];
    gridStyle.lineWidth = 0.5f;
    
//    graph.borderLineStyle = axisStyle;
//    graph.paddingLeft = 20.0;
//    graph.paddingRight = 20.0;
//    graph.paddingTop = 20.0;
//    graph.paddingBottom = 20.0;
    
    axisTextStyle.color = [CPTColor blackColor];
    
    //////////////////////////////////////////////////////////
    // X-axis parameters setting
    //////////////////////////////////////////////////////////

    CPTXYAxisSet *axisSet = (id)graph.axisSet;
    axisSet.xAxis.axisLineStyle = axisStyle;
    axisSet.xAxis.majorGridLineStyle = gridStyle;
    axisSet.xAxis.minorGridLineStyle = gridStyle;
    axisSet.xAxis.majorTickLength = 7.0f;

    //maybe change this when rotated
    // add plot for forecast values if they exist
    if((self.selectedMapUnit == Unit1 && self.chartData.ForecastUnit1Data !=nil && self.chartData.ForecastUnit1Data.count>0) || (self.selectedMapUnit == Unit2 && self.chartData.ForecastUnit2Data !=nil && self.chartData.ForecastUnit2Data.count>0)) {
        
        axisSet.xAxis.majorIntervalLength = CPTDecimalFromFloat(oneDay*2.0f);
        axisSet.xAxis.minorTicksPerInterval = 1;
    }
    else
    {
        axisSet.xAxis.majorIntervalLength = CPTDecimalFromFloat(oneDay);
        axisSet.xAxis.minorTicksPerInterval = 0;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"M/d"];
    [dateFormatter setTimeZone:[[NSTimeZone alloc] initWithName:@"UTC"]];
    CPTTimeFormatter *timeFormatter = [[CPTTimeFormatter alloc] initWithDateFormatter:dateFormatter];
    axisSet.xAxis.labelFormatter = timeFormatter;    
    
  //  axisSet.xAxis.orthogonalCoordinateDecimal = CPTDecimalFromFloat(1000.0);
    if(self.selectedMapUnit == Unit1)
    {
        //axisSet.xAxis.orthogonalCoordinateDecimal = CPTDecimalFromFloat(self.chartData.minUnit1YValue);
        axisSet.yAxis.orthogonalCoordinateDecimal = CPTDecimalFromFloat([self computeGraphMinTime:self.chartData.minUnit1XValue]);
    }
    else // is Unit2
    {
        //axisSet.xAxis.orthogonalCoordinateDecimal = CPTDecimalFromFloat(self.chartData.minUnit2YValue);
        axisSet.yAxis.orthogonalCoordinateDecimal = CPTDecimalFromFloat([self computeGraphMinTime:self.chartData.minUnit2XValue]);
    }
    axisSet.xAxis.orthogonalCoordinateDecimal = CPTDecimalFromFloat(self.chartMinDataValue);

    //axisSet.xAxis.orthogonalCoordinateDecimal = CPTDecimalFromString(@"1"); //added for date, adjust x line
  //  axisSet.xAxis.axisLineStyle.lineColor = [CPTColor blackColor];
  //  axisSet.xAxis.majorTickLineStyle = lineStyle;
 //   axisSet.xAxis.minorTickLineStyle = lineStyle;
 //   axisSet.xAxis.axisLineStyle = lineStyle;
 //   axisSet.xAxis.labelOffset = 3.0f;
    
    //////////////////////////////////////////////////////////
    // y-axis setting
    //////////////////////////////////////////////////////////
    
    axisSet.yAxis.axisLineStyle = axisStyle;
    axisSet.yAxis.majorGridLineStyle = gridStyle;
    axisSet.yAxis.majorTickLength = 7.0f;
    //axisSet.yAxis.preferredNumberOfMajorTicks = 4;
    axisSet.yAxis.labelFormatter = self.yAxisNumberFormatter;
    axisSet.yAxis.majorIntervalLength = CPTDecimalFromDouble(self.yAxisGridInterval);
    axisSet.yAxis.labelingOrigin = CPTDecimalFromDouble(self.chartMinDataValue);
    axisSet.yAxis.labelingPolicy = CPTAxisLabelingPolicyFixedInterval;
   
//    if(self.selectedMapUnit == Unit1)
//    {
//
//        axisSet.yAxis.majorIntervalLength = CPTDecimalFromFloat(200.f);
// 
//    }
//    else
//    {
//
//        axisSet.yAxis.majorIntervalLength = CPTDecimalFromFloat(1.f);
//        
//    }
    
    // set upper x axis
    if((self.selectedMapUnit == Unit1 && self.chartData.ForecastUnit1Data !=nil && self.chartData.ForecastUnit1Data.count>0) || (self.selectedMapUnit == Unit2 && self.chartData.ForecastUnit2Data !=nil && self.chartData.ForecastUnit2Data.count>0))
    {
        CPTXYAxis *upperX = [(CPTXYAxis *)[CPTXYAxis alloc] initWithFrame:CGRectZero];
        upperX.coordinate = CPTCoordinateX;
        upperX.plotSpace = graph.defaultPlotSpace;
        upperX.orthogonalCoordinateDecimal = CPTDecimalFromFloat(self.chartMaxDataValue);
        upperX.labelingPolicy = CPTAxisLabelingPolicyFixedInterval;
        if(self.selectedMapUnit == Unit1)
        {
            RCDateTimeDataPoint *dataPoint = [[self.chartData ObservedUnit1Data] objectAtIndex:(self.chartData.ObservedUnit1Data.count-1)];
            upperX.labelingOrigin = CPTDecimalFromDouble([dataPoint.XValue timeIntervalSinceReferenceDate]);
            
        }
        else // is unit2
        {
            RCDateTimeDataPoint *dataPoint = [[self.chartData ObservedUnit2Data] objectAtIndex:(self.chartData.ObservedUnit2Data.count-1)];
            upperX.labelingOrigin = CPTDecimalFromDouble([dataPoint.XValue timeIntervalSinceReferenceDate]);
        }
        upperX.majorIntervalLength = CPTDecimalFromFloat(oneDay*20);
        upperX.minorTicksPerInterval = 0;
        upperX.axisLineStyle = axisStyle;
        upperX.majorTickLength = 7.0f;
        upperX.labelFormatter = nil;
        upperX.tickDirection = CPTSignPositive;
        
        NSMutableArray *newAxes = [graph.axisSet.axes mutableCopy];
        [newAxes addObject:upperX];
        graph.axisSet.axes = newAxes;
        
        CPTMutableTextStyle *labelStyleObserved = [CPTMutableTextStyle textStyle];
        labelStyleObserved.color = [CPTColor colorWithComponentRed:0.0 green:0.478 blue:1.0 alpha:1.0]; // greenColor];
        //labelStyleObserved.fontName = @"Helvetica";
        labelStyleObserved.fontSize = 14.0f;
        //graph.titleTextStyle = titleStyle;
        //graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
        //graph.titleDisplacement = CGPointMake(0.0f, 10.0f);

        
        CPTTextLayer *textLayer = [[CPTTextLayer alloc] initWithText:@"Observed" style:labelStyleObserved];

        CPTMutableTextStyle *labelStylePredicted = [CPTMutableTextStyle textStyle];
        labelStylePredicted.color = [CPTColor colorWithComponentRed:0.498 green:0.89 blue:0.98 alpha:1.0];// blueColor];
        //labelStyleObserved.fontName = @"Helvetica";
        labelStylePredicted.fontSize = 14.0f;
        //graph.titleTextStyle = titleStyle;
        //graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
        //graph.titleDisplacement = CGPointMake(0.0f, 10.0f);

        CPTTextLayer *textLayerPredicted = [[CPTTextLayer alloc] initWithText:@"Predicted" style:labelStylePredicted];

        
        CPTLayerAnnotation *annotation = [[CPTLayerAnnotation alloc] initWithAnchorLayer:graph.plotAreaFrame.plotArea];
        annotation.rectAnchor = CPTRectAnchorTopLeft;
        annotation.displacement = CGPointMake(10, 0);
        annotation.contentLayer = textLayer;
        annotation.contentAnchorPoint = CGPointMake(0, 0);//bottom left
        
        
        CPTLayerAnnotation *annotationPredicted = [[CPTLayerAnnotation alloc] initWithAnchorLayer:graph.plotAreaFrame.plotArea];
        annotationPredicted.rectAnchor = CPTRectAnchorTopRight;
        annotationPredicted.displacement = CGPointMake(-10, 0);
        annotationPredicted.contentLayer = textLayerPredicted;
        annotationPredicted.contentAnchorPoint = CGPointMake(1,0);//bottom right

        [graph.plotAreaFrame addAnnotation:annotation];
        [graph.plotAreaFrame addAnnotation:annotationPredicted];
        
    }
    else if(self.isLogPlot)
    {
        CPTXYAxis *upperX = [(CPTXYAxis *)[CPTXYAxis alloc] initWithFrame:CGRectZero];
        upperX.coordinate = CPTCoordinateX;
        upperX.plotSpace = graph.defaultPlotSpace;
        upperX.orthogonalCoordinateDecimal = CPTDecimalFromFloat(self.chartMaxDataValue);
        upperX.labelingPolicy = CPTAxisLabelingPolicyFixedInterval;
        
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar ];
        [calendar setTimeZone:[[NSTimeZone alloc] initWithName:@"UTC"]];
        
        NSDateComponents *components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:self.logDate];
        [components setHour:12];
        [components setMinute:0];
        [components setSecond:0];
        
        upperX.labelingOrigin = CPTDecimalFromDouble([[calendar dateFromComponents:components] timeIntervalSinceReferenceDate]);
        
        upperX.majorIntervalLength = CPTDecimalFromFloat(oneDay*20);
        upperX.minorTicksPerInterval = 0;
        upperX.axisLineStyle = axisStyle;
        upperX.majorTickLength = 3.0f;
        upperX.labelFormatter = nil;
        upperX.tickDirection = CPTSignPositive;
        
        NSMutableArray *newAxes = [graph.axisSet.axes mutableCopy];
        [newAxes addObject:upperX];
        graph.axisSet.axes = newAxes;
        
        CPTMutableTextStyle *labelStyleDate = [CPTMutableTextStyle textStyle];
        labelStyleDate.color = [CPTColor colorWithComponentRed:0.0 green:0.478 blue:1.0 alpha:1.0];// blueColor];
        //labelStyleObserved.fontName = @"Helvetica";
        labelStyleDate.fontSize = 12.0f;
        //graph.titleTextStyle = titleStyle;
        //graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
        //graph.titleDisplacement = CGPointMake(0.0f, 10.0f);
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [dateFormatter setLocale:usLocale];
        
        CPTTextLayer *textLayer = [[CPTTextLayer alloc] initWithText:[NSString stringWithFormat:@"Log date: %@",[dateFormatter stringFromDate:self.logDate]] style:labelStyleDate];
        
        CPTLayerAnnotation *annotation = [[CPTLayerAnnotation alloc] initWithAnchorLayer:graph.plotAreaFrame.plotArea];
        annotation.rectAnchor = CPTRectAnchorTop;
        annotation.displacement = CGPointMake(-textLayer.sizeThatFits.width/2.0, 3);
        annotation.contentLayer = textLayer;
        annotation.contentAnchorPoint = CGPointMake(0, 0);//bottom left
        
        
//        CPTLayerAnnotation *annotationPredicted = [[CPTLayerAnnotation alloc] initWithAnchorLayer:graph.plotAreaFrame.plotArea];
//        annotationPredicted.rectAnchor = CPTRectAnchorTopRight;
//        annotationPredicted.displacement = CGPointMake(-10, 0);
//        annotationPredicted.contentLayer = textLayerPredicted;
//        annotationPredicted.contentAnchorPoint = CGPointMake(1,0);//bottom right
        
        [graph.plotAreaFrame addAnnotation:annotation];
        //[graph.plotAreaFrame addAnnotation:annotationPredicted];
    }
}

//- (IBAction)unit2BarButtonItemPressed:(UIBarButtonItem *)sender {
//    self.selectedMapUnit = Unit2;
//    self.unit2BarButtonItem.style = UIBarButtonItemStyleDone;
//    self.unit1BarButtonItem.style = UIBarButtonItemStyleBordered;
//    [self initPlot];
//}
//- (IBAction)unit1BarButtonItemPressed:(UIBarButtonItem *)sender {
//    self.selectedMapUnit = Unit1;
//    unit1BarButtonItem.style = UIBarButtonItemStyleDone;
//    unit2BarButtonItem.style = UIBarButtonItemStyleBordered;
//    [self initPlot];
//}
//- (IBAction)favoriteBarButtonItemPressed:(UIBarButtonItem *)sender {
//}

-(NSTimeInterval)computeGraphMinTime:(NSTimeInterval)minTime {
    NSDate *thisDate = [NSDate dateWithTimeIntervalSinceReferenceDate:minTime];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar ];
    [calendar setTimeZone:[[NSTimeZone alloc] initWithName:@"UTC"]];
    
    NSDateComponents *thisDateComps = [calendar components:NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:thisDate];
    [thisDateComps setHour:0];
    [thisDateComps setMinute:0];
    [thisDateComps setSecond:0];
    return [[calendar dateFromComponents:thisDateComps] timeIntervalSinceReferenceDate];
}

-(NSTimeInterval)computeGraphMaxTime:(NSTimeInterval)maxTime {
    maxTime += 60*60*24; // add one day
    return [self computeGraphMinTime:maxTime];
}
-(void)computeYAxisParameters
{
    double yMin;
    double yMax;
    self.yAxisNumberFormatter = [[NSNumberFormatter alloc] init];
    if(self.selectedMapUnit == Unit1)
    {
        yMin = self.chartData.minUnit1YValue;
        yMax = self.chartData.maxUnit1YValue;
    }
    else // is Unit2
    {
        yMin = self.chartData.minUnit2YValue;
        yMax = self.chartData.maxUnit2YValue;
    }
    if(yMax - yMin < .5)
    {
        yMax += 1.5;
        yMin -= 1.5;
        self.chartMinDataValue = [RCNumberRounder RoundDownTenth:yMin];
        self.chartMaxDataValue = [RCNumberRounder RoundUpTenth:yMax];
        [self.yAxisNumberFormatter setMaximumFractionDigits:1];
        [self.yAxisNumberFormatter setMinimumFractionDigits:1];
        self.yAxisGridInterval = [RCNumberRounder RoundUpTenth:((chartMaxDataValue-chartMinDataValue)/4)];
        self.chartMaxDataValue = self.chartMinDataValue + 4*self.yAxisGridInterval;
    }
    else if(yMax - yMin < 5.0)
    {
        self.chartMinDataValue = [RCNumberRounder RoundDownTenth:yMin];
        self.chartMaxDataValue = [RCNumberRounder RoundUpTenth:yMax];
        [self.yAxisNumberFormatter setMaximumFractionDigits:1];
        [self.yAxisNumberFormatter setMinimumFractionDigits:1];
        self.yAxisGridInterval = [RCNumberRounder RoundUpTenth:((chartMaxDataValue-chartMinDataValue)/4)];
        self.chartMaxDataValue = self.chartMinDataValue + 4*self.yAxisGridInterval;
    }
    else if(yMax - yMin < 10.0)
    {
        self.chartMinDataValue = [RCNumberRounder RoundDown:yMin];
        self.chartMaxDataValue = [RCNumberRounder RoundUp:yMax];
        [self.yAxisNumberFormatter setMaximumFractionDigits:1];
        [self.yAxisNumberFormatter setMinimumFractionDigits:1];
        self.yAxisGridInterval = [RCNumberRounder RoundUp:((chartMaxDataValue-chartMinDataValue)/4)];
        self.chartMaxDataValue = self.chartMinDataValue + 4*self.yAxisGridInterval;
    }
    else if(yMax - yMin < 100.0)
    {
        self.chartMinDataValue = [RCNumberRounder RoundDownTens:yMin];
        self.chartMaxDataValue = [RCNumberRounder RoundUpTens:yMax];
        [self.yAxisNumberFormatter setMaximumFractionDigits:0];
        [self.yAxisNumberFormatter setMinimumFractionDigits:0];
        self.yAxisGridInterval = [RCNumberRounder RoundUpTens:((chartMaxDataValue-chartMinDataValue)/4)];
        self.chartMaxDataValue = self.chartMinDataValue + 4*self.yAxisGridInterval;
    }
    else 
    {
        self.chartMinDataValue = [RCNumberRounder RoundDownHundreds:yMin];
        self.chartMaxDataValue = [RCNumberRounder RoundUpHundreds:yMax];
        [self.yAxisNumberFormatter setMaximumFractionDigits:0];
        [self.yAxisNumberFormatter setMinimumFractionDigits:0];
        self.yAxisGridInterval = [RCNumberRounder RoundUpTens:((chartMaxDataValue-chartMinDataValue)/4)];
        self.chartMaxDataValue = self.chartMinDataValue + 4*self.yAxisGridInterval;
    }

}
- (void)bookmarkAction:(UIBarButtonItem*)sender{
    if(!chartItem.isFavorite)
    {
        chartItem.isFavorite = YES;
        [RCModelShared updateFavorites];
        if([[RCGlobalSingleton globalSingleton] favoritesTableView] != nil)
            [[[RCGlobalSingleton globalSingleton] favoritesTableView] reloadData];
        [self.navigationItem rightBarButtonItem].style=UIBarButtonItemStyleDone;
        
        UIImage *image= [UIImage imageNamed:@"726-star-selected.png"];
        _addBookmarkButton.image = image;
    }
}
//- (IBAction)rateItAction:(UIBarButtonItem *)sender {
//    RCAddLogViewController *rateItViewController =(RCAddLogViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"AddLog"];
//    rateItViewController.chartItem = self.chartItem;
//    
////    int selectedRowIndex =indexPath.row;
////    //[self.chartItemTableView indexPathForSelectedRow].row;
////    RCChartItem *chartItem;
////    if(self.isFiltered)
////    {
////        chartItem= (RCChartItem *)[self.filteredTableData objectAtIndex:selectedRowIndex];
////    }
////    else
////    {
////        chartItem= (RCChartItem *)[[RCModelShared chartItems] objectAtIndex:selectedRowIndex];
////    }
////    
////    scatterPlotViewController.PEType = chartItem.peType;
////    scatterPlotViewController.stationID = chartItem.stationID;
////    scatterPlotViewController.itemName = chartItem.name;
////    scatterPlotViewController.hidesBottomBarWhenPushed = true;
////    scatterPlotViewController.chartItem = chartItem;
//    
//    [self.navigationController presentModalViewController:rateItViewController animated:YES];
//}

- (IBAction)unitSwitchButtonTouch:(UIButton *)sender {
    if(self.selectedMapUnit == Unit1)
    {
        self.selectedMapUnit = Unit2;
        [RCModelShared setLastSelectedUnit:Unit2];
        self.plotLabel.text = self.chartData.YAxisLabelUnit2;
        [self.unitSwitchButton setImage:self.btnImageUnit2 forState:UIControlStateNormal];
    }
    else
    {
        self.selectedMapUnit = Unit1;
        [RCModelShared setLastSelectedUnit:Unit1];
        self.plotLabel.text = self.chartData.YAxisLabelUnit1;
        [self.unitSwitchButton setImage:self.btnImageUnit1 forState:UIControlStateNormal];
    }
    [self initPlot];
}
- (IBAction)logItTouched:(UIButton *)sender {
    RCAddLogViewController *rateItViewController =(RCAddLogViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"AddLog"];
    rateItViewController.chartItem = self.chartItem;
    
    //    int selectedRowIndex =indexPath.row;
    //    //[self.chartItemTableView indexPathForSelectedRow].row;
    //    RCChartItem *chartItem;
    //    if(self.isFiltered)
    //    {
    //        chartItem= (RCChartItem *)[self.filteredTableData objectAtIndex:selectedRowIndex];
    //    }
    //    else
    //    {
    //        chartItem= (RCChartItem *)[[RCModelShared chartItems] objectAtIndex:selectedRowIndex];
    //    }
    //
    //    scatterPlotViewController.PEType = chartItem.peType;
    //    scatterPlotViewController.stationID = chartItem.stationID;
    //    scatterPlotViewController.itemName = chartItem.name;
    //    scatterPlotViewController.hidesBottomBarWhenPushed = true;
    //    scatterPlotViewController.chartItem = chartItem;
    
    [self.navigationController presentViewController:rateItViewController animated:YES completion:NULL];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[_HUD removeFromSuperview];
	//[HUD release];
	_HUD = nil;
}
@end
