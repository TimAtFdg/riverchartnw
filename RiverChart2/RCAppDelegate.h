//
//  RCAppDelegate.h
//  RiverChart2
//
//  Created by Tim Fiez on 12/28/12.
//  Copyright (c) 2012 Tim Fiez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
