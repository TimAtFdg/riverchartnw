//
//  RCGlobalSingleton.h
//  RiverChart2
//
//  Created by Tim Fiez on 4/15/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface RCGlobalSingleton : NSObject
@property (nonatomic, retain) UITableView *favoritesTableView;
@property (nonatomic, retain) UITableView *logTableView;
@property (nonatomic) NetworkStatus networkStatus;
+(id)globalSingleton;
@end
