//
//  RCChartData.h
//  RiverChart2
//
//  Created by Tim Fiez on 2/13/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RCChartData : NSObject
@property(nonatomic) BOOL DataRetrievedSuccessfully;
@property(nonatomic,strong,readonly) NSString* ErrorMessage;
@property(nonatomic,strong,readonly) NSString* TimeLabel;
@property(nonatomic,strong,readonly) NSString* YAxisLabelUnit1;
@property(nonatomic,strong,readonly) NSString* YAxisLabelUnit2;
@property double minUnit1YValue;
@property double maxUnit1YValue;
@property double minUnit2YValue;
@property double maxUnit2YValue;
@property NSTimeInterval minUnit1XValue;
@property NSTimeInterval maxUnit1XValue;
@property NSTimeInterval minUnit2XValue;
@property NSTimeInterval maxUnit2XValue;
-(NSMutableArray *)ForecastUnit1Data;
-(NSMutableArray *)ForecastUnit2Data;
-(NSMutableArray *)ObservedUnit1Data;
-(NSMutableArray *)ObservedUnit2Data;
-(id)initWithDictionary:(NSDictionary*)chartDataData;

@end
