//
//  RCDataPoint.h
//  RiverChart2
//
//  Created by Tim Fiez on 2/15/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RCDataPoint : NSObject
@property (nonatomic) double XValue;
@property (nonatomic) double YValue;
-(id)initWithXValueAndYValue:(double)XValue YValue:(double)YValue;
@end
