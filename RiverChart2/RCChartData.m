//
//  RCChartData.m
//  RiverChart2
//
//  Created by Tim Fiez on 2/13/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCChartData.h"
#import "RCDateTimeDataPoint.h"

//static NSDateFormatter *sUserVisibleDateFormatter = nil;
static NSDateFormatter *sRFC3339DateFormatter = nil;

@interface RCChartData()
@property (strong, nonatomic) NSMutableArray *forecastUnit1Data;
@property (strong, nonatomic) NSMutableArray *forecastUnit2Data;
@property (strong, nonatomic) NSMutableArray *observedUnit1Data;
@property (strong, nonatomic) NSMutableArray *observedUnit2Data;


- (NSDate *)dateForRFC3339DateTimeString:(NSString *)rfc3339DateTimeString;
@end

@implementation RCChartData
@synthesize DataRetrievedSuccessfully;
@synthesize TimeLabel;
@synthesize YAxisLabelUnit1;
@synthesize YAxisLabelUnit2;
@synthesize ErrorMessage;
@synthesize forecastUnit1Data = _forecastUnit1Data;
@synthesize forecastUnit2Data = _forecastUnit2Data;
@synthesize observedUnit1Data = _observedUnit1Data;
@synthesize observedUnit2Data = _observedUnit2Data;

@synthesize minUnit1XValue;
@synthesize maxUnit1XValue;

@synthesize minUnit2XValue;
@synthesize maxUnit2XValue;

@synthesize minUnit1YValue;
@synthesize maxUnit1YValue;

@synthesize minUnit2YValue;
@synthesize maxUnit2YValue;

//-(double)minDouble:(double)currentValue currentMin:(double)currentMin
//{
//    if(currentValue<currentMin)
//        return 
//}

-(id)initWithDictionary:(NSDictionary*)chartDataData
{
    if(self = [super init])
    {
        NSNumber *val = [chartDataData valueForKey:@"DataRetrievedSuccessfully"];
        DataRetrievedSuccessfully = [val boolValue];
        //DataRetrievedSuccessfully = [(NSNumber)[chartDataData valueForKey:@"DataRetrievedSuccessfully"];
        TimeLabel = [chartDataData valueForKey:@"TimeLabel"];
        YAxisLabelUnit1 = [chartDataData valueForKey:@"YAxisLabelUnit1"];
        YAxisLabelUnit2 = [chartDataData valueForKey:@"YAxisLabelUnit2"];
        ErrorMessage = [chartDataData valueForKey:@"ErrorMessage"];
        
        double tempYValue = 0;
        //NSDate *tempXValue = 0;
        
        minUnit1YValue = DBL_MAX;
        maxUnit1YValue = -1;
        minUnit2YValue = DBL_MAX;
        maxUnit2YValue = -1;

        
        NSArray *ForecastUnit1DataArray = [chartDataData objectForKey:@"ForecastUnit1Data"];
        if(ForecastUnit1DataArray!= (id)[NSNull null] && [ForecastUnit1DataArray count]>0)
        {
            //minUnit1XValue = 0;
            //maxUnit1XValue = 0;
            
            self.forecastUnit1Data = [[NSMutableArray alloc] init];
            for(NSDictionary *dataPointResult in ForecastUnit1DataArray)
            {
                tempYValue = [[dataPointResult objectForKey:@"YValue"] doubleValue];
                //tempXValue = [self dateForRFC3339DateTimeString:[dataPointResult valueForKey:@"XValue"]];
                
                minUnit1YValue = minUnit1YValue>tempYValue ? tempYValue : minUnit1YValue;
                maxUnit1YValue = maxUnit1YValue <tempYValue ? tempYValue : maxUnit1YValue;
                
                [self.forecastUnit1Data addObject:[[RCDateTimeDataPoint alloc] initWithXValueAndYValue:[self dateForRFC3339DateTimeString:[dataPointResult valueForKey:@"XValue"]] YValue:[[dataPointResult objectForKey:@"YValue"] doubleValue]]];
            }
        }
        NSArray *ForecastUnit2DataArray = [chartDataData objectForKey:@"ForecastUnit2Data"];
        if(ForecastUnit2DataArray != (id)[NSNull null] && [ForecastUnit2DataArray count]>0)
        {
            self.forecastUnit2Data = [[NSMutableArray alloc] init];
            for(NSDictionary *dataPointResult in ForecastUnit2DataArray)
            {
                tempYValue = [[dataPointResult objectForKey:@"YValue"] doubleValue];
                minUnit2YValue = minUnit2YValue>tempYValue ? tempYValue : minUnit2YValue;
                maxUnit2YValue = maxUnit2YValue <tempYValue ? tempYValue : maxUnit2YValue;

                [self.forecastUnit2Data addObject:[[RCDateTimeDataPoint alloc] initWithXValueAndYValue:[self dateForRFC3339DateTimeString:[dataPointResult valueForKey:@"XValue"]] YValue:[[dataPointResult objectForKey:@"YValue"] doubleValue]]];
            }
        }
        NSArray *ObservedUnit1DataArray = [chartDataData objectForKey:@"ObservedUnit1Data"];
        if(ObservedUnit1DataArray != (id)[NSNull null] && [ObservedUnit1DataArray count]>0)
        {
            self.observedUnit1Data = [[NSMutableArray alloc] init];
            for(NSDictionary *dataPointResult in ObservedUnit1DataArray)
            {
                //NSString *xValueString = [dataPointResult valueForKey:@"XValue"];
                //NSDate *xDateValue = [self dateForRFC3339DateTimeString:xValueString];
                //double temp = [[dataPointResult valueForKey:@"XValue"] doubleValue] - 10000000000000000.0;

                tempYValue = [[dataPointResult objectForKey:@"YValue"] doubleValue];                
                minUnit1YValue = minUnit1YValue>tempYValue ? tempYValue : minUnit1YValue;
                maxUnit1YValue = maxUnit1YValue <tempYValue ? tempYValue : maxUnit1YValue;
                
                [self.observedUnit1Data addObject:[[RCDateTimeDataPoint alloc] initWithXValueAndYValue:[self dateForRFC3339DateTimeString:[dataPointResult valueForKey:@"XValue"]] YValue:[[dataPointResult objectForKey:@"YValue"] doubleValue]]];
                
                
              //  [self.observedUnit1Data addObject:[[RCDataPoint alloc] initWithXValueAndYValue:([[dataPointResult valueForKey:@"XValue"] doubleValue] - 10000000000000000.0) YValue:[[dataPointResult objectForKey:@"YValue"] doubleValue]]];
            }
        }
        NSArray *ObservedUnit2DataArray = [chartDataData objectForKey:@"ObservedUnit2Data"];
        if(ObservedUnit2DataArray != (id)[NSNull null] && [ObservedUnit2DataArray count]>0)
        {
            self.observedUnit2Data = [[NSMutableArray alloc] init];
            for(NSDictionary *dataPointResult in ObservedUnit2DataArray)
            {
                tempYValue = [[dataPointResult objectForKey:@"YValue"] doubleValue];
                minUnit2YValue = minUnit2YValue>tempYValue ? tempYValue : minUnit2YValue;
                maxUnit2YValue = maxUnit2YValue <tempYValue ? tempYValue : maxUnit2YValue;
                
                [self.observedUnit2Data addObject:[[RCDateTimeDataPoint alloc] initWithXValueAndYValue:[self dateForRFC3339DateTimeString:[dataPointResult valueForKey:@"XValue"]] YValue:[[dataPointResult objectForKey:@"YValue"] doubleValue]]];
            }
        }
        // get time ranges based on known sorting of component data
        minUnit1XValue = DBL_MAX;
        maxUnit1XValue = -1;
        minUnit2XValue = DBL_MAX;
        maxUnit2XValue = -1;
        
        NSTimeInterval tempTimeInterval;
        
        if(self.observedUnit1Data != nil && [self.observedUnit1Data count]>0)
        {
            tempTimeInterval=[((RCDateTimeDataPoint*)[self.observedUnit1Data objectAtIndex:0]).XValue timeIntervalSinceReferenceDate];
            minUnit1XValue = minUnit1XValue>tempTimeInterval ? tempTimeInterval : minUnit1XValue;
            
            tempTimeInterval=[((RCDateTimeDataPoint*)[self.observedUnit1Data lastObject]).XValue timeIntervalSinceReferenceDate];
            maxUnit1XValue = maxUnit1XValue<tempTimeInterval ? tempTimeInterval : maxUnit1XValue;
            
        }
        if(self.forecastUnit1Data != nil && [self.forecastUnit1Data count]>0)
        {
            tempTimeInterval=[((RCDateTimeDataPoint*)[self.forecastUnit1Data objectAtIndex:0]).XValue timeIntervalSinceReferenceDate];
            minUnit1XValue = minUnit1XValue>tempTimeInterval ? tempTimeInterval : minUnit1XValue;
            
            tempTimeInterval=[((RCDateTimeDataPoint*)[self.forecastUnit1Data lastObject]).XValue timeIntervalSinceReferenceDate];
            maxUnit1XValue = maxUnit1XValue<tempTimeInterval ? tempTimeInterval : maxUnit1XValue;
            
        }
        if(self.observedUnit2Data != nil && [self.observedUnit2Data count]>0)
        {
            tempTimeInterval=[((RCDateTimeDataPoint*)[self.observedUnit2Data objectAtIndex:0]).XValue timeIntervalSinceReferenceDate];
            minUnit2XValue = minUnit2XValue>tempTimeInterval ? tempTimeInterval : minUnit2XValue;
            
            tempTimeInterval=[((RCDateTimeDataPoint*)[self.observedUnit2Data lastObject]).XValue timeIntervalSinceReferenceDate];
            maxUnit2XValue = maxUnit2XValue<tempTimeInterval ? tempTimeInterval : maxUnit2XValue;
            
        }
        if(self.forecastUnit2Data != nil && [self.forecastUnit2Data count]>0)
        {
            tempTimeInterval=[((RCDateTimeDataPoint*)[self.forecastUnit2Data objectAtIndex:0]).XValue timeIntervalSinceReferenceDate];
            minUnit2XValue = minUnit2XValue>tempTimeInterval ? tempTimeInterval : minUnit2XValue;
            
            tempTimeInterval=[((RCDateTimeDataPoint*)[self.forecastUnit2Data lastObject]).XValue timeIntervalSinceReferenceDate];
            maxUnit2XValue = maxUnit2XValue<tempTimeInterval ? tempTimeInterval : maxUnit2XValue;
            
        }
    }
    return self;
}

-(NSMutableArray*)ForecastUnit1Data
{
    return _forecastUnit1Data;
}
-(NSMutableArray*)ForecastUnit2Data
{
    return _forecastUnit2Data;
}
-(NSMutableArray*)ObservedUnit1Data
{
    return _observedUnit1Data;
}
-(NSMutableArray*)ObservedUnit2Data
{
    return _observedUnit2Data;
}

- (NSDate *)dateForRFC3339DateTimeString:(NSString *)rfc3339DateTimeString {
    /*
     Returns a user-visible date time string that corresponds to the specified
     RFC 3339 date time string. Note that this does not handle all possible
     RFC 3339 date time strings, just one of the most common styles.
     */
    
    // If the date formatters aren't already set up, create them and cache them for reuse.
    //static NSDateFormatter *sRFC3339DateFormatter = nil;
    if (sRFC3339DateFormatter == nil) {
        sRFC3339DateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        
        [sRFC3339DateFormatter setLocale:enUSPOSIXLocale];
        [sRFC3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
        [sRFC3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
    
    // Convert the RFC 3339 date time string to an NSDate.
    NSDate *date = [sRFC3339DateFormatter dateFromString:rfc3339DateTimeString];
    return date;
}


//    NSString *userVisibleDateTimeString;
//    if (date != nil) {
//        if (sUserVisibleDateFormatter == nil) {
//            sUserVisibleDateFormatter = [[NSDateFormatter alloc] init];
//            [sUserVisibleDateFormatter setDateStyle:NSDateFormatterShortStyle];
//            [sUserVisibleDateFormatter setTimeStyle:NSDateFormatterShortStyle];
//        }
//        // Convert the date object to a user-visible date string.
//        userVisibleDateTimeString = [sUserVisibleDateFormatter stringFromDate:date];
//    }
//    return userVisibleDateTimeString;
//}
@end
