//
//  RCFetchChartData.h
//  RiverChart2
//
//  Created by Tim Fiez on 2/13/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RCChartData.h"

@interface RCFetchChartData : NSObject
+(RCChartData*)GetForStation:(NSString*)stationID PEType:(NSString*)PEType;
@end
