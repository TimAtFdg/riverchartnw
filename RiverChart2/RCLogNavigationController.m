//
//  RCLogNavigationController.m
//  RiverChart2
//
//  Created by Tim on 2/2/14.
//  Copyright (c) 2014 Tim Fiez. All rights reserved.
//

#import "RCLogNavigationController.h"

@interface RCLogNavigationController ()

@end

@implementation RCLogNavigationController



-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
//    if (self) {
//        // Custom initialization
//        UIImage *selectedImage = [UIImage imageNamed:@"704-compose-selected"];
//        self.logTabBarItem.selectedImage = selectedImage; //[UIImage imageNamed:@"704-compose-selected.png"];
//    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //if(self.logTabBarItem.selectedImage==nil)
    self.logTabBarItem.selectedImage = [UIImage imageNamed:@"704-compose-selected"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
