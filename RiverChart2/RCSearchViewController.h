//
//  RCSearchViewController.h
//  RiverChart2
//
//  Created by Tim Fiez on 4/4/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "MBProgressHUD.h"

@interface RCSearchViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *chartItemTableView;
//@property(nonatomic,strong) MBProgressHUD *HUD;
//- (void)hudWasHidden:(MBProgressHUD *)hud;;
@end
