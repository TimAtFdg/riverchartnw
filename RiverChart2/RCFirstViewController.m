//
//  RCFirstViewController.m
//  RiverChart2
//
//  Created by Tim Fiez on 12/28/12.
//  Copyright (c) 2012 Tim Fiez. All rights reserved.
//

#import "RCFirstViewController.h"

@interface RCFirstViewController ()
@property (weak, nonatomic) IBOutlet UIButton *myButton;

@end

@implementation RCFirstViewController
@synthesize myButton;
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setMyButton:nil];
    [super viewDidUnload];
}
@end
