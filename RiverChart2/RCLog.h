//
//  RCLog.h
//  RiverChart2
//
//  Created by Tim on 12/20/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RCStation;

@interface RCLog : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSNumber * rating;
@property (nonatomic, retain) NSString * stationID;
@property (nonatomic, retain) NSString * peType;
@property (nonatomic, retain) RCStation *rcStation_relationship;

@end
