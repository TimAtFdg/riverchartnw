//
//  RCDataPoint.m
//  RiverChart2
//
//  Created by Tim Fiez on 2/15/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCDataPoint.h"

@implementation RCDataPoint
@synthesize XValue=_XValue;
@synthesize YValue=_YValue;

-(id)initWithXValueAndYValue:(double)XValue YValue:(double)YValue
{
    if(self = [super init])
    {
        self.XValue = XValue;
        self.YValue = YValue;
    }
    return self;
}

@end
