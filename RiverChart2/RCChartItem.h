//
//  RCChartItem.h
//  RiverChart2
//
//  Created by Tim Fiez on 5/27/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RCStation;

@interface RCChartItem : NSManagedObject

@property (nonatomic) BOOL isFavorite;
@property (nonatomic, retain) NSString * peType;
@property (nonatomic, retain) NSString * stationID;
@property (nonatomic, retain) RCStation *rcStation_relationship;

@end
