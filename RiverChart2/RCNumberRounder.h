//
//  RCNumberRounder.h
//  RiverChart2
//
//  Created by Tim Fiez on 4/1/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RCNumberRounder : NSObject
+(double)RoundUpTenth:(double)number;
+(double)RoundDownTenth:(double)number;
+(double)RoundDown:(double)number;
+(double)RoundUp:(double)number;
+(double)RoundUpTens:(double)number;
+(double)RoundDownTens:(double)number;
+(double)RoundUpHundreds:(double)number;
+(double)RoundDownHundreds:(double)number;
@end
