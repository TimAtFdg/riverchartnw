//
//  RCScatterPlotViewController.h
//  RiverChart2
//
//  Created by Tim Fiez on 1/26/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "RCConstants.h"
#import "RCChartItem.h"
#import "MBProgressHUD.h"

@interface RCScatterPlotViewController : UIViewController <CPTPlotDataSource>
//UINavigationBarDelegate
//@property (strong, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UILabel *plotLabel;
@property (strong, nonatomic) NSString *PEType;
@property (strong, nonatomic) NSString *itemName;
@property (strong, nonatomic) NSString *stationID;
@property (weak, nonatomic) IBOutlet UIView *errorView;
@property (strong, nonatomic) IBOutlet CPTGraphHostingView *hostView;
@property (weak, nonatomic) IBOutlet UIButton *unitSwitchButton;
@property (weak, nonatomic) RCChartItem *chartItem;
@property BOOL isLogPlot;
@property (strong, nonatomic) NSDate *logDate;
@property(nonatomic,strong) MBProgressHUD *HUD;
- (IBAction)unitSwitchButtonTouch:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *logItButton;
- (IBAction)logItTouched:(UIButton *)sender;
- (void)hudWasHidden:(MBProgressHUD *)hud;
//@property (weak, nonatomic) IBOutlet UIBarButtonItem *favoriteBarButtonItem;

//@property (strong, nonatomic) IBOutlet UIBarButtonItem *unit2BarButtonItem;

//@property (strong, nonatomic) IBOutlet UIBarButtonItem *unit1BarButtonItem;
//@property (strong, nonatomic) IBOutlet UIBarButtonItem *rateItBarButtonItem;
//@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;

@end
