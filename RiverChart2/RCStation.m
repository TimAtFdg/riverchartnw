//
//  RCStation.m
//  RiverChart2
//
//  Created by Tim Fiez on 5/24/13.
//  Copyright (c) 2013 Tim Fiez. All rights reserved.
//

#import "RCStation.h"
#import "RCChartItem.h"


@implementation RCStation

@dynamic stationID;
@dynamic name;
@dynamic hasDischargeData;
@dynamic hasStageData;
@dynamic rcLog_relationship;
@dynamic rcChartItem_relationship;

-(id)setAttributesWithDictionary:(NSDictionary *)stationData
{
    if(self)
    {
        self.name = [stationData valueForKey:@"name"];
        self.stationID = [stationData valueForKey:@"stationID"];
        self.hasDischargeData = (BOOL)[stationData valueForKey:@"hasDischargeData"];
        self.hasStageData = (BOOL)[stationData valueForKey:@"hasStageData"];
    }
    return self;
}
@end
